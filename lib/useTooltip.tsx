import { Signal, signal, useSignal } from "@preact/signals";
import { FunctionComponent } from "preact";
import { useCallback, useEffect, useId } from "preact/hooks";
import { createPortal } from "preact/compat";

export interface TooltipProps {
  title: string;
}

const currentTooltip = signal<string | undefined>(undefined);

function getPortalRoot() {
  const id = "tooltips-portal-root";
  let div = document.getElementById(id);

  if (!div) {
    div = document.createElement("div");
    div.id = id;
    document.body.append(div);
  }

  return div;
}

function getAdjustedBoundingClientRect(elem: HTMLElement) {
  const { x, y, width, height } = elem.getBoundingClientRect();
  const { scrollX, scrollY } = window;
  return { x: x + scrollX, y: y + scrollY, width, height };
}

interface TooltipInPortalProps {
  title: string;
  parent: HTMLElement;
}

const TooltipInPortal: FunctionComponent<TooltipInPortalProps> = ({
  title,
  parent,
}) => {
  const viewportWidth = document.documentElement.clientWidth;
  const { x, y, width, height } = getAdjustedBoundingClientRect(parent);
  const middleX = x + width / 2;
  const dir = middleX < viewportWidth / 2 ? "right" : "left";

  return createPortal(
    <span
      class={`absolute z-10 pointer-events-none bg-gray-100 rounded text-black px-2 py-1 whitespace-pre text-sm font-normal -translate-y-1/2 ${
        {
          left: "-translate-x-full -ml-1",
          right: "ml-1",
        }[dir]
      }`}
      style={{
        left: dir === "left" ? x : x + width,
        top: y + height / 2,
      }}
    >
      {title}
    </span>,
    getPortalRoot(),
  );
};

const useTooltip = (
  title?: string,
  showTooltip?: Signal<boolean | undefined>,
) => {
  const id = useId();
  const showing = useSignal(false);
  const elem = useSignal<HTMLElement | null>(null);
  const ref = useCallback(
    (x: HTMLElement | null) => {
      elem.value = x;
    },
    [elem],
  );

  useEffect(() => {
    const x = elem.value;
    if (!x) return;

    x.addEventListener("mouseenter", show);
    x.addEventListener("mouseleave", hide);
    x.addEventListener("focusin", show);
    x.addEventListener("focusout", hide);

    return () => {
      x.removeEventListener("mouseenter", show);
      x.removeEventListener("mouseleave", hide);
      x.removeEventListener("focusin", show);
      x.removeEventListener("focusout", hide);
    };
  }, [elem.value]);

  useEffect(() => {
    if (!showing.value) return;

    const escapeListener = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        hide();
      }
    };

    globalThis.addEventListener("keydown", escapeListener);

    return () => {
      globalThis.removeEventListener("keydown", escapeListener);
    };
  }, [showing.value]);

  const show = () => {
    showing.value = true;
    currentTooltip.value = id;
  };
  const hide = () => {
    showing.value = false;
    currentTooltip.value = undefined;
  };

  const isShowing = showTooltip?.value ??
    (showing.value && currentTooltip.value === id);

  return {
    ref,
    node: isShowing && elem.value && title
      ? <TooltipInPortal title={title} parent={elem.value} />
      : null,
    targetNode: elem.value,
  };
};

export default useTooltip;
