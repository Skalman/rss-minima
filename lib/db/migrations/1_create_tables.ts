export default {
  id: "1_create_tables",
  sql: `
    CREATE TABLE user (
      userId INTEGER PRIMARY KEY,
      username TEXT NOT NULL UNIQUE,
      usernameDisplay TEXT NOT NULL,
      passwordHash TEXT NOT NULL
    ) STRICT;
    
    CREATE TABLE userSession (
      userSessionId TEXT PRIMARY KEY,
      userId INTEGER NOT NULL,
      createdDateTime TEXT NOT NULL,
      FOREIGN KEY (userId) REFERENCES user(userId)
    ) STRICT;
    
    CREATE TABLE feed (
      feedId INTEGER PRIMARY KEY,
      url TEXT NOT NULL UNIQUE,
      name TEXT NOT NULL,
      lastUpdateDateTime TEXT NOT NULL
    ) STRICT;

    CREATE TABLE feedItem (
      feedItemId INTEGER PRIMARY KEY,
      feedId INTEGER NOT NULL,
      url TEXT NOT NULL,
      feedItemDateTime TEXT NOT NULL,
      content TEXT NULL,
      attachmentUrl TEXT NULL,
      FOREIGN KEY (feedId) REFERENCES feed(feedId)
    ) STRICT;

    CREATE TABLE userFeed (
      feedId INTEGER NOT NULL,
      userId INTEGER NOT NULL,
      updateFrequencyMinutes INTEGER NOT NULL,
      addedDateTime TEXT NOT NULL,
      UNIQUE (feedId, userId),
      FOREIGN KEY (feedId) REFERENCES feed(feedId),
      FOREIGN KEY (userId) REFERENCES user(userId)
    ) STRICT;

    CREATE TABLE userFeedItemRead (
      feedItemId INTEGER,
      userId INTEGER,
      UNIQUE (feedItemId, userId),
      FOREIGN KEY (feedItemId) REFERENCES feedItem(feedItemId),
      FOREIGN KEY (userId) REFERENCES user(userId)
    ) STRICT;
    `,
};
