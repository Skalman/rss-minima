import IconCircleDot from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle-dot.tsx";
import IconCircle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle.tsx";
import IconInfoCircle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/info-circle.tsx";
import { memo } from "preact/compat";
import { FunctionComponent } from "preact";
import useTooltip from "../lib/useTooltip.tsx";
import {
  highlightItem,
  markAsRead,
  markAsUnread,
} from "../state/operations.ts";
import { Feed, FeedItem } from "../state/State.ts";
import useAppState from "../state/useAppState.ts";
import CopyButton from "./CopyButton.tsx";
import IconButton from "./IconButton.tsx";
import Link from "./Link.tsx";
import { useContext } from "preact/hooks";
import listDisplayContext from "../lib/listDisplayContext.ts";

interface ListFeedEntryProps {
  feed: Feed;
  feedEntry: FeedItem;
}

const ListFeedEntry: FunctionComponent<ListFeedEntryProps> = ({
  feed: { feedId, copyFormat },
  feedEntry,
}) => {
  const { isRead, url, title, feedItemId } = feedEntry;
  const { dispatch } = useAppState();
  const listDisplay = useContext(listDisplayContext);

  const toggleRead = useTooltip(isRead ? "Mark as unread" : "Mark as read");
  const openUrl = useTooltip(`Open ${url}`);
  const details = useTooltip("Show details");

  let copyText: string, copyTitle: string;

  if (copyFormat === "yt-dlp" || copyFormat === "yt-dlp-sound") {
    const escapedUrl = url.replace(/[\\"]/g, "\\$&");
    copyText = copyFormat === "yt-dlp"
      ? `yt-dlp "${escapedUrl}"`
      : `yt-dlp -x "${escapedUrl}"`;
    copyTitle = `Copy command: ${copyText}`;
  } else {
    copyText = url;
    copyTitle = `Copy URL: ${url}`;
  }

  return (
    <div
      class={`flex gap-2 ${
        !isRead && listDisplay.entries === "all" ? "font-bold" : ""
      }`}
    >
      <div class="whitespace-pre">
        <CopyButton
          title={copyTitle}
          text={copyText}
        />

        {toggleRead.node}
        <IconButton
          ref={toggleRead.ref}
          icon={isRead ? IconCircle : IconCircleDot}
          onClick={() =>
            dispatch(isRead ? markAsUnread : markAsRead, {
              feedId,
              feedItemId,
            })}
        />
      </div>

      {openUrl.node}
      <div
        ref={openUrl.ref}
        class="flex-shrink overflow-hidden whitespace-nowrap overflow-ellipsis"
      >
        <Link
          href={url}
          target="_blank"
        >
          {title}
        </Link>
      </div>

      {details.node}
      <IconButton
        ref={details.ref}
        icon={IconInfoCircle}
        onClick={() =>
          dispatch(highlightItem, {
            type: "feedItem",
            feedId,
            feedItemId,
          })}
      />
    </div>
  );
};

export default memo(ListFeedEntry);
