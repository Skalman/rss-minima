import {
  passwordHash,
  passwordNeedsRehash,
  passwordVerify,
} from "../password_hash.ts";
import database from "./database.ts";

export interface UserSessionSchema {
  userSessionId: string;
  userId: number;
  createdDateTime: string;
}

interface CreateSessionOptions {
  usernameDisplay: string;
  password: string;
}

type CreateSessionResult =
  | {
    sessionId: string;
    usernameDisplay: string;
    result: "logged-in" | "created-user";
  }
  | {
    sessionId?: undefined;
    usernameDisplay?: undefined;
    result: "bad-password";
  };

export function selectUserIdBySessionId(userSessionId: string) {
  const row = database
    .query<[number]>(
      "SELECT userId FROM userSession WHERE userSessionId = :userSessionId",
      { userSessionId },
    )
    .at(0);
  return row?.[0];
}

export function createUserSession({
  usernameDisplay,
  password,
}: CreateSessionOptions): CreateSessionResult {
  const username = usernameDisplay.replace(/[ :()#_-]/g, "").toLowerCase();
  const user = database
    .queryEntries<{ userId: number; passwordHash: string }>(
      "SELECT userId, passwordHash FROM user WHERE username = :username",
      { username },
    )
    .at(0);

  const pepper = "";

  if (!user) {
    const hash = passwordHash(pepper, password);
    database
      .prepareQuery(
        `
          INSERT INTO user (username, usernameDisplay, passwordHash)
          VALUES (:username, :usernameDisplay, :passwordHash)
        `,
      )
      .execute({ username, usernameDisplay, passwordHash: hash });
    return createUserSession({ usernameDisplay, password });
  }

  if (!passwordVerify(pepper, password, user.passwordHash)) {
    return { result: "bad-password" };
  }

  if (passwordNeedsRehash(user.passwordHash)) {
    const newPasswordHash = passwordHash(pepper, password);
    database
      .prepareQuery(
        "UPDATE user SET passwordHash = :passwordHash WHERE username = :username",
      )
      .execute({ username, passwordHash: newPasswordHash });
  }

  const sessionId = crypto.randomUUID();
  database
    .prepareQuery(
      `
        INSERT INTO userSession (userSessionId, userId, createdDateTime)
        VALUES (:userSessionId, :userId, :createdDateTime)
      `,
    )
    .execute({
      userSessionId: sessionId,
      userId: user.userId,
      createdDateTime: new Date().toISOString(),
    });

  return { result: "logged-in", sessionId, usernameDisplay };
}
