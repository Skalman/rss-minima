import { IS_BROWSER } from "$fresh/runtime.ts";
import { JSX } from "preact";

export interface SelectProps<T>
  extends Omit<JSX.HTMLAttributes<HTMLSelectElement>, "value"> {
  value: T;
  options: { value: T; label?: string }[];
  onOptionChange: (value: T) => void;
  variant?: "default" | "transparent";
}

const Select = function <T,>({
  value,
  options,
  class: className,
  onOptionChange,
  variant = "default",
  onChange,
  disabled,
  ...rest
}: SelectProps<T>) {
  return (
    <select
      {...rest}
      class={`px-3 py-1.5
        text-base text-white
        bg-black bg-clip-padding bg-no-repeat
        border border-solid border-gray-500
        rounded
        transition
        ease-in-out
        focus:(outline-none ring border-gray-300) ${
        {
          default: "",
          transparent:
            "not-hover:not-focus:(bg-transparent border-transparent)",
        }[variant]
      } ${className}`}
      onChange={function (this: HTMLSelectElement, e) {
        onChange?.call(this, e);
        onOptionChange(options[e.currentTarget.selectedIndex].value);
      }}
      disabled={!IS_BROWSER || disabled}
    >
      {options.map((x) => (
        <option selected={x.value === value}>{x.label ?? `${x.value}`}</option>
      ))}
    </select>
  );
};

export default Select;
