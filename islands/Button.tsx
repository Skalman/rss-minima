import { FunctionComponent, JSX } from "preact";
import { forwardRef } from "preact/compat";
import { IS_BROWSER } from "$fresh/runtime.ts";
import { tw } from "twind";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

interface ButtonPropsOnly {
  variant: "primary" | "secondary" | "tertiary";
  size?: "sm" | "md";
  rounded?: "all" | "left" | "right" | "none";
  class?: JSX.HTMLAttributes["class"];
}

export interface ButtonProps
  extends
    Omit<JSX.HTMLAttributes<HTMLButtonElement>, "size">,
    ButtonPropsOnly {}

interface SignalLike<T> {
  value: T;
  peek(): T;
  subscribe(fn: (value: T) => void): () => void;
}

function unwrapSignal<T>(maybeSignal?: T | SignalLike<T>): T | undefined {
  if (!maybeSignal) {
    return maybeSignal;
  }

  if (typeof maybeSignal === "object" && "peek" in maybeSignal) {
    return maybeSignal.value;
  }

  return maybeSignal;
}

function getButtonClass({
  variant,
  size = "md",
  class: className,
  rounded = "all",
}: ButtonPropsOnly) {
  return tw`${
    {
      sm: "px-4 py-1.5",
      md: "px-6 py-2.5",
    }[size]
  } border ${
    {
      primary:
        "border-transparent text-white bg-blue(600 hover:700 focus:700 active:800)",
      secondary:
        "border-transparent text-white bg-gray(600 hover:700 focus:700 active:800)",
      tertiary:
        "border-blue-700 text(blue-500 white(hover:& focus:& active:&)) bg(black blue(hover:700 focus:700 active:800))",
    }[variant]
  } ${
    {
      all: "rounded",
      left: "rounded-l",
      right: "rounded-r",
      none: "",
    }[rounded]
  } shadow(md lg(hover:& focus:& active:&)) font-medium text-xs leading-tight uppercase focus:(outline-none ring) transition duration-150 ease-in-out disabled:opacity-60 ${
    unwrapSignal(className) ?? ""
  }`;
}

const Button: FunctionComponent<ButtonProps> = forwardRef<
  HTMLButtonElement,
  ButtonProps
>(function (
  { variant, size, rounded, class: className, type, disabled, ...rest },
  ref,
) {
  return (
    <button
      ref={ref}
      type={type ?? "button"}
      disabled={!IS_BROWSER || disabled}
      {...keyboardProps.focusable}
      class={getButtonClass({
        variant,
        size,
        rounded,
        class: className,
      })}
      {...rest}
    />
  );
});

export default Button;
