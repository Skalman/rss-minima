import database from "./database.ts";
import { FeedSchema } from "./feed.ts";
import { QueryParameterSet, RowObject } from "sqlite";

export type CopyFormat = "yt-dlp" | "yt-dlp-sound";

export interface UserFeedSchema {
  feedId: number;
  userId: number;
  customName: string | null;
  updateFrequencyMinutes: number;
  addedDateTime: string;
  expanded: 0 | 1;
  sort: number | null;
  enabled: 0 | 1;
  copyFormat: null | CopyFormat;
}

export interface UserFeedDomain {
  feedId: number;
  url: string;
  name: string;
  originalName: string;
  lastUpdatedDateTime: string;
  lastError?: string;
  lastErrorDateTime?: string;
  updateFrequencyMinutes: number;
  expanded: boolean;
  enabled: boolean;
  copyFormat?: CopyFormat;
  addedDateTime: string;
  items: UserFeedItemDomain[];
}

export interface UserFeedItemDomain {
  feedItemId: number;
  title: string;
  url: string;
  feedItemDateTime: string;
  content?: string;
  attachmentUrl?: string;
  isRead: boolean;
}

function selectUserFeedsInternal({
  userId,
  feedId,
}: {
  userId: number;
  feedId?: number;
}) {
  const { condition, params } = feedId === undefined
    ? {
      condition: "userFeed.userId = :userId",
      params: { userId },
    }
    : {
      condition: "userFeed.feedId = :feedId AND userFeed.userId = :userId",
      params: { feedId, userId },
    };
  const feeds = database
    .queryEntries<{
      feedId: FeedSchema["feedId"];
      url: FeedSchema["url"];
      name: FeedSchema["name"];
      lastUpdatedDateTime: FeedSchema["lastUpdatedDateTime"];
      lastError: FeedSchema["lastError"];
      lastErrorDateTime: FeedSchema["lastErrorDateTime"];
      customName: UserFeedSchema["customName"];
      updateFrequencyMinutes: UserFeedSchema["updateFrequencyMinutes"];
      expanded: UserFeedSchema["expanded"];
      addedDateTime: UserFeedSchema["addedDateTime"];
      enabled: UserFeedSchema["enabled"];
      copyFormat: UserFeedSchema["copyFormat"];
    }>(
      `
        SELECT
          feed.feedId,
          feed.url,
          feed.name,
          feed.lastUpdatedDateTime,
          feed.lastError,
          feed.lastErrorDateTime,
          userFeed.customName,
          userFeed.updateFrequencyMinutes,
          userFeed.expanded,
          userFeed.addedDateTime,
          userFeed.enabled,
          userFeed.copyFormat
        FROM feed
          JOIN userFeed
            ON feed.feedId = userFeed.feedId
        WHERE ${condition}
        ORDER BY COALESCE(userFeed.sort, 999999), userFeed.addedDateTime
      `,
      params,
    )
    .map<UserFeedDomain>(
      ({
        name,
        customName,
        expanded,
        enabled,
        copyFormat,
        lastError,
        lastErrorDateTime,
        ...rest
      }) => ({
        name: customName ?? name,
        expanded: expanded === 1,
        enabled: enabled === 1,
        originalName: name,
        ...rest,
        copyFormat: copyFormat ?? undefined,
        lastError: lastError ?? undefined,
        lastErrorDateTime: lastErrorDateTime ?? undefined,
        items: [],
      }),
    );
  const byFeedId = new Map(feeds.map((x) => [x.feedId, x]));

  interface FeedItemEntry extends RowObject {
    feedItemId: number;
    feedId: number;
    title: string;
    url: string;
    feedItemDateTime: string;
    content: string | null;
    attachmentUrl: string | null;
    isRead: 0 | 1;
  }
  const feedItems = database.queryEntries<FeedItemEntry>(
    `
      SELECT
        feedItem.feedId,
        feedItem.feedItemId,
        feedItem.title,
        feedItem.url,
        feedItem.feedItemDateTime,
        feedItem.content,
        feedItem.attachmentUrl,
        CASE WHEN userFeedItemRead.userId IS NULL
          THEN 0
          ELSE 1
        END AS isRead
      FROM userFeed
        JOIN feedItem
          ON userFeed.feedId = feedItem.feedId
        LEFT JOIN userFeedItemRead
          ON userFeedItemRead.feedItemId = feedItem.feedItemId
          AND userFeedItemRead.userId = userFeed.userId
      WHERE ${condition}
      ORDER BY feedItem.feedItemDateTime DESC
      `,
    params,
  );

  for (
    const {
      feedId,
      feedItemId,
      title,
      url,
      feedItemDateTime,
      content,
      attachmentUrl,
      isRead,
    } of feedItems
  ) {
    const feed = byFeedId.get(feedId);
    if (!feed) {
      throw new Error("Strange problem 715723");
    }

    feed.items.push({
      feedItemId,
      title,
      url,
      feedItemDateTime,
      content: content ?? undefined,
      attachmentUrl: attachmentUrl ?? undefined,
      isRead: !!isRead,
    });
  }

  return feeds;
}

export function selectUserFeed(opts: { userId: number; feedId: number }) {
  return selectUserFeedsInternal(opts).at(0);
}

export function selectUserFeeds(userId: number) {
  return selectUserFeedsInternal({ userId });
}

export function insertUserFeed({
  userId,
  feedId,
  updateFrequencyMinutes,
}: {
  userId: number;
  feedId: number;
  updateFrequencyMinutes: number;
}) {
  database
    .prepareQuery(
      `
        INSERT INTO userFeed (userId, feedId, updateFrequencyMinutes, addedDateTime)
        VALUES (:userId, :feedId, :updateFrequencyMinutes, :addedDateTime)
      `,
    )
    .execute({
      userId,
      feedId,
      updateFrequencyMinutes,
      addedDateTime: new Date().toISOString(),
    });
}

export function deleteUserFeed({
  userId,
  feedId,
}: {
  userId: number;
  feedId: number;
}) {
  database
    .prepareQuery(
      `DELETE FROM userFeed WHERE userId = :userId AND feedId = :feedId`,
    )
    .execute({ userId, feedId });
}

export function updateUserFeed({
  userId,
  feedId,
  expanded,
  enabled,
  customName,
  updateFrequencyMinutes,
  copyFormat,
}: {
  userId: number;
  feedId: number;
  expanded?: boolean;
  enabled?: boolean;
  /** `undefined` causes it not to be set, `null` sets it to null. */
  customName?: string | null;
  updateFrequencyMinutes?: number;
  /** `undefined` causes it not to be set, `null` sets it to null. */
  copyFormat?: CopyFormat | null;
}) {
  const params: QueryParameterSet = { userId, feedId };
  const setSyntax: string[] = [];

  if (expanded !== undefined) {
    params.expanded = expanded ? 1 : 0;
    setSyntax.push("expanded = :expanded");
  }

  if (enabled !== undefined) {
    params.enabled = enabled ? 1 : 0;
    setSyntax.push("enabled = :enabled");
  }

  if (customName !== undefined) {
    params.customName = customName;
    setSyntax.push("customName = :customName");
  }

  if (updateFrequencyMinutes !== undefined) {
    params.updateFrequencyMinutes = updateFrequencyMinutes;
    setSyntax.push("updateFrequencyMinutes = :updateFrequencyMinutes");
  }

  if (copyFormat !== undefined) {
    params.copyFormat = copyFormat;
    setSyntax.push("copyFormat = :copyFormat");
  }

  if (!setSyntax.length) {
    throw new Error(
      "Either 'expanded', 'enabled', 'customName' or 'updateFrequencyMinutes' is required",
    );
  }

  database
    .prepareQuery(
      `
        UPDATE userFeed
        SET ${setSyntax.join(", ")}
        WHERE
          userId = :userId
          AND feedId = :feedId
      `,
    )
    .execute(params);
}

export function getUserFeedByUrl(userId: number, url: string) {
  const row = database
    .queryEntries<{
      feedId: FeedSchema["feedId"];
      name: FeedSchema["name"];
      url: FeedSchema["url"];
      lastUpdatedDateTime: FeedSchema["lastUpdatedDateTime"];
      updateFrequencyMinutes: UserFeedSchema["updateFrequencyMinutes"] | null;
      addedDateTime: UserFeedSchema["addedDateTime"] | null;
    }>(
      `
        SELECT
          feed.feedId,
          feed.name,
          feed.url,
          feed.lastUpdatedDateTime,
          userFeed.updateFrequencyMinutes,
          userFeed.addedDateTime
        FROM feed
          LEFT JOIN userFeed
            ON feed.feedId = userFeed.feedId
            AND userFeed.userId = :userId
        WHERE url = :url
      `,
      { userId, url },
    )
    .at(0);

  if (!row) {
    return;
  }

  return {
    feed: {
      feedId: row.feedId,
      name: row.name,
      url: row.url,
      lastUpdatedDateTime: row.lastUpdatedDateTime,
    },
    userFeed: row.updateFrequencyMinutes === null ? undefined : {
      updateFrequencyMinutes: row.updateFrequencyMinutes,
      addedDateTime: row.addedDateTime ?? undefined,
    },
  };
}
