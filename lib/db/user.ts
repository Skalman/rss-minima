import { QueryParameterSet } from "sqlite";
import database from "./database.ts";

export interface UserSchema {
  userId: number;
  username: string;
  usernameDisplay: string;
  passwordHash: string;
  settings: string | null;
}

export function selectUser(userId: number) {
  return database
    .queryEntries<{
      settings: UserSchema["settings"];
    }>(
      `
        SELECT settings
        FROM user
        WHERE userId = :userId`,
      { userId },
    )
    .map((x) => ({
      settings: x.settings ?? undefined,
    }))
    .at(0);
}

export function updateUser({
  userId,
  settings,
}: {
  userId: number;
  settings?: string;
}) {
  const params: QueryParameterSet = { userId };
  const setSyntax: string[] = [];

  if (settings !== undefined) {
    params.settings = settings;
    setSyntax.push("settings = :settings");
  }

  database
    .prepareQuery(
      `
        UPDATE user
        SET ${setSyntax.join(", ")}
        WHERE userId = :userId`,
    )
    .execute(params);
}
