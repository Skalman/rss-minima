import { QueryParameterSet } from "sqlite";
import database from "./database.ts";

export interface FeedSchema {
  feedId: number;
  url: string;
  name: string;
  /** Last successful fetch. */
  lastUpdatedDateTime: string;
  /** Last error. Is `null` if the previous fetch did not cause an error. */
  lastError: string | null;
  lastErrorDateTime: string | null;
}

export function insertFeed({
  url,
  name,
  lastUpdatedDateTime,
}: {
  url: string;
  name: string;
  lastUpdatedDateTime: string;
}) {
  database
    .prepareQuery(
      `
        INSERT INTO feed (url, name, lastUpdatedDateTime)
        VALUES (:url, :name, :lastUpdatedDateTime)
      `,
    )
    .execute({ url, name, lastUpdatedDateTime });

  return database.lastInsertRowId;
}

function selectFeed(
  by: "feedId" | "url",
  params: { feedId: number } | { url: string },
): FeedSchema | undefined {
  return database
    .queryEntries<{
      feedId: FeedSchema["feedId"];
      name: FeedSchema["name"];
      url: FeedSchema["url"];
      lastUpdatedDateTime: FeedSchema["lastUpdatedDateTime"];
      lastError: FeedSchema["lastError"];
      lastErrorDateTime: FeedSchema["lastErrorDateTime"];
    }>(
      `
        SELECT feedId, name, url, lastUpdatedDateTime
        FROM feed
        WHERE ${by} = :${by}
      `,
      params,
    )
    .at(0);
}

export function selectFeedById(feedId: number) {
  return selectFeed("feedId", { feedId });
}

export function selectFeedByUrl(url: string) {
  return selectFeed("url", { url });
}

export function updateFeed({
  feedId,
  url,
  name,
  lastUpdatedDateTime,
  lastError,
  lastErrorDateTime,
}: { feedId: number } & Partial<FeedSchema>) {
  const params: QueryParameterSet = { feedId };
  const setSyntax: string[] = [];

  if (url !== undefined) {
    params.url = url;
    setSyntax.push("url = :url");
  }

  if (name !== undefined) {
    params.name = name;
    setSyntax.push("name = :name");
  }

  if (lastUpdatedDateTime !== undefined) {
    params.lastUpdatedDateTime = lastUpdatedDateTime;
    setSyntax.push("lastUpdatedDateTime = :lastUpdatedDateTime");
  }

  if (lastError !== undefined) {
    params.lastError = lastError;
    setSyntax.push("lastError = :lastError");
  }

  if (lastErrorDateTime !== undefined) {
    params.lastErrorDateTime = lastErrorDateTime;
    setSyntax.push("lastErrorDateTime = :lastErrorDateTime");
  }

  database
    .prepareQuery(
      `
        UPDATE feed
        SET ${setSyntax.join(", ")}
        WHERE feedId = :feedId
      `,
    )
    .execute(params);
}
