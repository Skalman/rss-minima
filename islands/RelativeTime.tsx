import { FunctionComponent } from "preact";
import { useEffect } from "preact/hooks";
import { useSignal } from "@preact/signals";

interface RelativeTimeProps {
  time: string;
}

const units = {
  second: 1000,
  minute: 1000 * 60,
  hour: 1000 * 60 * 60,
  day: 1000 * 60 * 60 * 24,
};

const RelativeTime: FunctionComponent<RelativeTimeProps> = ({ time }) => {
  const diff = Date.now() - new Date(time).getTime();
  const unit = diff < units.minute
    ? "second"
    : diff < units.hour
    ? "minute"
    : diff < units.day
    ? "hour"
    : "day";
  const unitMs = units[unit];

  const rerender = useSignal(0);

  useEffect(() => {
    const nextChange = unitMs - (diff % unitMs);

    const timer = setTimeout(() => {
      rerender.value++;
    }, nextChange);

    return () => {
      clearTimeout(timer);
    };
  }, [diff, unitMs, rerender.value]);

  return (
    <time title={time}>
      {new Intl.RelativeTimeFormat("en").format(
        -Math.floor(diff / unitMs),
        unit,
      )}
    </time>
  );
};

export default RelativeTime;
