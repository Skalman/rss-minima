export default {
  id: "3_add_feedItem.title",
  sql: `
    PRAGMA foreign_keys=OFF;

    CREATE TABLE new_feedItem (
      feedItemId INTEGER PRIMARY KEY,
      feedId INTEGER NOT NULL,
      title TEXT NOT NULL,
      url TEXT NOT NULL,
      feedItemDateTime TEXT NOT NULL,
      content TEXT NULL,
      attachmentUrl TEXT NULL,
      FOREIGN KEY (feedId) REFERENCES feed(feedId)
    ) STRICT;

    INSERT INTO new_feedItem
    SELECT
      feedItemId,
      feedId,
      '[title]',
      url,
      feedItemDateTime,
      content,
      attachmentUrl
    FROM feedItem;

    DROP TABLE feedItem;

    ALTER TABLE new_feedItem RENAME TO feedItem;

    PRAGMA foreign_key_check;
    PRAGMA foreign_keys=ON;
    `,
};
