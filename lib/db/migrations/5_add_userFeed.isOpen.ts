export default {
  id: "5_add_userFeed.isOpen",
  sql: `
    ALTER TABLE userFeed
    ADD COLUMN isOpen INTEGER NOT NULL DEFAULT 1;
    `,
};
