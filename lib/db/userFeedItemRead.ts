import database from "./database.ts";

export interface UserFeedItemReadSchema {
  feedItemId: number;
  userId: number;
}

export function insertUserFeedItemRead({
  userId,
  feedItemIds,
}: {
  userId: number;
  feedItemIds: number[];
}) {
  for (const feedItemId of feedItemIds) {
    database
      .prepareQuery(
        `
          INSERT INTO userFeedItemRead (feedItemId, userId)
          VALUES (:feedItemId, :userId)
          ON CONFLICT DO NOTHING`,
      )
      .execute({ feedItemId, userId });
  }
}

export function deleteUserFeedItemRead({
  userId,
  feedItemIds,
}: {
  userId: number;
  feedItemIds: number[];
}) {
  for (const feedItemId of feedItemIds) {
    database
      .prepareQuery(
        `
          DELETE FROM userFeedItemRead
          WHERE
            feedItemId = :feedItemId
            AND userId = :userId`,
      )
      .execute({ feedItemId, userId });
  }
}
