export const sqliteDatabase = Deno.env.get("MINIMA_SQLITE_DATABASE") ??
  "data/rss-minima.sqlite";

export const port = +(Deno.env.get("MINIMA_PORT") ?? 8000);

export const automaticMigrations =
  Deno.env.get("MINIMA_AUTOMATIC_MIGRATIONS") === "true";
