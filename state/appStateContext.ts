import { createContext } from "preact";
import State from "./State.ts";

const appStateContext = createContext<State>(undefined!);
export default appStateContext;
