import { computed, ReadonlySignal } from "@preact/signals";

const memoComputed = <T>(
  fn: () => T,
  equal: (a: T, b: T) => boolean,
): ReadonlySignal<T> => {
  const signal = computed(fn);
  let last: { value: T } | undefined;

  return computed<T>(() => {
    const current = signal.value;
    if (last) {
      if (equal(last.value, current)) {
        return last.value;
      }
    }

    last = { value: current };
    return current;
  });
};

export default memoComputed;
