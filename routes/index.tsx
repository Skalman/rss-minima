import { Head } from "$fresh/runtime.ts";
import { Handler, PageProps } from "$fresh/server.ts";
import GlobalStyle from "../components/GlobalStyle.tsx";
import Root from "../islands/Root.tsx";
import { getSessionFromCookie } from "../lib/cookie.ts";
import { selectUser } from "../lib/db/user.ts";
import { selectUserFeeds } from "../lib/db/userFeed.ts";
import { selectUserIdBySessionId } from "../lib/db/userSession.ts";

interface IndexProps {
  userId: number;
  settings?: string;
  feeds: ReturnType<typeof selectUserFeeds>;
}

export const handler: Handler<IndexProps> = async (req, ctx) => {
  const userSessionId = getSessionFromCookie(req.headers);
  const userId = userSessionId && selectUserIdBySessionId(userSessionId);
  if (!userId) {
    return Response.redirect(new URL("./login", req.url));
  }

  const user = selectUser(userId);
  const feeds = selectUserFeeds(userId);
  return await ctx.render({
    userId,
    settings: user?.settings,
    feeds,
  });
};

export default function Index({
  data: { feeds, settings },
}: PageProps<IndexProps>) {
  return (
    <>
      <Head>
        <title>RSS Minima</title>
        <GlobalStyle />
      </Head>
      <Root feeds={feeds} settings={settings} />
    </>
  );
}
