import { createContext } from "preact";
import { ListDisplay } from "../state/State.ts";

const listDisplayContext = createContext<ListDisplay>({
  feeds: "all",
  entries: "all",
});

export default listDisplayContext;
