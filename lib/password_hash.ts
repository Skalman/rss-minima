// TODO Use a real hashing algorithm.
// Inspired by PHP's password hash functions.

export function passwordHash(pepper: string, password: string) {
  return `:${pepper}:${password}`;
}

export function passwordVerify(pepper: string, password: string, hash: string) {
  return passwordHash(pepper, password) === hash;
}

export function passwordNeedsRehash(_hash: string) {
  return false;
}
