export default {
  id: "11_add_userFeed.copyFormat",
  sql: `
    ALTER TABLE userFeed
    ADD COLUMN copyFormat TEXT NULL;
    `,
};
