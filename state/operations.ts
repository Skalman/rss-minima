import produce, { Draft, nothing } from "immer";
import { Signal } from "@preact/signals";
import getFrontEndApi from "../lib/getFrontEndApi.ts";
import State, {
  Feed,
  FeedItem,
  HighlightedItemId,
  ListDisplay,
} from "./State.ts";
import { CopyFormat, UserFeedDomain } from "../lib/db/userFeed.ts";
import { UserFeedItemDomain } from "../lib/db/userFeed.ts";

declare type ValidRecipeReturnType<State> =
  | State
  | void
  | undefined
  | (State extends undefined ? typeof nothing : never);

interface FullFeedItemId {
  feedId: number;
  feedItemId: number;
}

function update<T>(
  signal: Signal<T>,
  callback: (x: Draft<T>) => ValidRecipeReturnType<Draft<T>>,
) {
  signal.value = produce(signal.value, callback);
}

const api = getFrontEndApi();

export function toggleAddFeedForm(state: State) {
  const {
    addFeed: { submission },
    highlightedItemId,
  } = state;

  if (highlightedItemId.value?.type === "addFeed") {
    highlightedItemId.value = undefined;
  } else {
    highlightItem(state, { type: "addFeed" });
  }

  if (submission.value?.state === "error") {
    submission.value = undefined;
  }
}

export async function addFeed(
  { feeds, addFeed: { submission }, highlightedItemId }: State,
  url: string,
) {
  submission.value = { state: "in-progress", url };

  try {
    const feed = await api.subscribe(url);
    const { feedId } = feed;

    submission.value = undefined;
    update(feeds, (x) => {
      const index = x.findIndex((x) => x.feedId === feedId);
      if (index === -1) {
        x.push(mapFeed(feed));
      } else {
        x.splice(index, 1, mapFeed(feed));
      }
    });
    submission.value = undefined;
    highlightedItemId.value = { type: "feed", feedId };
  } catch (error) {
    submission.value = { state: "error", error };
    console.error(error);
  }
}

interface Settings {
  listDisplay?: ListDisplay;
}
export function loadSettings({ listDisplay }: State, settingsJson?: string) {
  if (settingsJson) {
    const settings: Settings = JSON.parse(settingsJson);
    if (settings.listDisplay) {
      listDisplay.value = settings.listDisplay;
    }
  }
}

export async function saveSettings({ listDisplay }: State) {
  const settings: Settings = { listDisplay: listDisplay.value };
  await api.saveSettings(JSON.stringify(settings));
}

type ApiFeed = UserFeedDomain;
type ApiFeedItem = UserFeedItemDomain;

function mapFeedItem({
  feedItemId,
  title,
  url,
  feedItemDateTime,
  content,
  attachmentUrl,
  isRead,
}: ApiFeedItem): FeedItem {
  return {
    feedItemId,
    title,
    url,
    dateTime: feedItemDateTime,
    content,
    attachmentUrl,
    isRead,
  };
}

function mapFeed({
  feedId,
  url,
  name,
  originalName,
  updateFrequencyMinutes,
  copyFormat,
  expanded,
  enabled,
  addedDateTime,
  items,
  lastUpdatedDateTime,
  lastError,
  lastErrorDateTime,
}: ApiFeed): Feed {
  return {
    feedId,
    url,
    name,
    originalName,
    updateFrequencyMinutes,
    copyFormat,
    expanded,
    enabled,
    addedDateTime,
    lastUpdatedDateTime,
    lastError,
    lastErrorDateTime,
    items: items.map(mapFeedItem),
  };
}

export async function fetchFeedList({ feeds, fetchingFeeds }: State) {
  fetchingFeeds.value = { state: "in-progress" };

  const newFeeds = await api.getFeeds();

  fetchingFeeds.value = undefined;
  update(feeds, () => {
    return newFeeds.map(mapFeed);
  });
}

export function setInitialFeeds({ feeds }: State, initialFeeds: ApiFeed[]) {
  update(feeds, () => {
    return initialFeeds.map(mapFeed);
  });
}

export function highlightItem(
  { highlightedItemId, highlightedShowAllEntries }: State,
  object: HighlightedItemId,
  { showAllEntries, force }: { showAllEntries?: true; force?: true } = {},
) {
  const old = highlightedItemId.value;

  if (
    !force &&
    showAllEntries === undefined &&
    old &&
    old.type === object.type &&
    old.feedId === object.feedId &&
    old.feedItemId === object.feedItemId
  ) {
    highlightedItemId.value = undefined;
    highlightedShowAllEntries.value = false;
  } else {
    highlightedItemId.value = object;
    highlightedShowAllEntries.value = !!showAllEntries;
  }
}

export function highlightShowAllEntries({ highlightedShowAllEntries }: State) {
  highlightedShowAllEntries.value = true;
}

function markAs(
  { feeds }: State,
  mode: "read" | "unread",
  { feedId, feedItemIds }: { feedId: number; feedItemIds?: number[] },
) {
  const feedItemIdsSet = new Set(feedItemIds);
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (!feed) return;

    const feedItems = feedItemIds
      ? feed.items.filter((x) => feedItemIdsSet.has(x.feedItemId))
      : feed.items;

    for (const feedItem of feedItems) {
      feedItem.isRead = mode === "read";
    }

    api.markAs(
      mode,
      feedItemIds ?? feed.items.map(({ feedItemId }) => feedItemId),
    );
  });
}

export function markAsRead(
  state: State,
  { feedId, feedItemId }: FullFeedItemId,
) {
  markAs(state, "read", { feedId, feedItemIds: [feedItemId] });
}

export function markAsUnread(
  state: State,
  { feedId, feedItemId }: FullFeedItemId,
) {
  markAs(state, "unread", { feedId, feedItemIds: [feedItemId] });
}

export function markFeedAsRead(state: State, feedId: number) {
  markAs(state, "read", { feedId });
}

export function markFeedAsUnread(state: State, feedId: number) {
  markAs(state, "unread", { feedId });
}

export function setFeedExpanded(
  { feeds }: State,
  feedId: number,
  expanded: boolean,
) {
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (feed) {
      feed.expanded = expanded;
    }
  });

  api.setFeedExpanded(feedId, expanded);
}

const feedCustomNameDecounced = new Map<number, number>();
export function setFeedCustomName(
  { feeds }: State,
  feedId: number,
  customName?: string,
) {
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (feed) {
      feed.name = customName ?? feed.originalName;
    }
  });

  clearTimeout(feedCustomNameDecounced.get(feedId));
  feedCustomNameDecounced.set(
    feedId,
    setTimeout(async () => {
      feedCustomNameDecounced.delete(feedId);
      await api.setFeedCustomName({ feedId, customName });
    }, 1000),
  );
}

export async function setFeedUpdateFrequency(
  { feeds }: State,
  feedId: number,
  updateFrequencyMinutes: number,
) {
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (feed) {
      feed.updateFrequencyMinutes = updateFrequencyMinutes;
    }
  });

  await api.setFeedUpdateFrequency(feedId, updateFrequencyMinutes);
}

export async function setFeedCopyFormat(
  { feeds }: State,
  feedId: number,
  copyFormat?: CopyFormat,
) {
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (feed) {
      feed.copyFormat = copyFormat;
    }
  });

  await api.setFeedCopyFormat(feedId, copyFormat);
}

export async function setFeedEnabled(
  { feeds }: State,
  feedId: number,
  enabled: boolean,
) {
  update(feeds, (x) => {
    const feed = x.find((x) => x.feedId === feedId);
    if (feed) {
      feed.enabled = enabled;
    }
  });

  await api.setFeedEnabled(feedId, enabled);
}

export async function removeFeed(
  { removingFeed, feeds }: State,
  feedId: number,
) {
  removingFeed.value = { state: "in-progress", feedId };
  await api.unsubscribe(feedId);

  removingFeed.value = undefined;
  update(feeds, (x) => x.filter((x) => x.feedId !== feedId));
}

export async function refreshFeed(
  { feeds, refreshingFeeds }: State,
  feedId: number,
) {
  update(refreshingFeeds, (x) => {
    x.push(feedId);
  });

  try {
    const updatedFeed = await api.refreshFeed(feedId);

    update(feeds, (x) => {
      const index = x.findIndex((x) => x.feedId === feedId);
      mapFeed(updatedFeed);
      x.splice(index, 1, mapFeed(updatedFeed));
    });
  } finally {
    update(refreshingFeeds, (x) => x.filter((x) => x !== feedId));
  }
}
