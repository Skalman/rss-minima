import { JSX } from "preact/jsx-runtime";

interface TypographyProps extends JSX.HTMLAttributes<HTMLDivElement> {
  variant: "h1" | "h3";
}

type Variant = TypographyProps["variant"];

const variantComponents = {
  h1: "h1",
  h3: "h3",
} as const;

const variantStyles: Record<Variant, string> = {
  h1: "text-3xl",
  h3: "text-lg",
};

export default function Typography({ variant, ...rest }: TypographyProps) {
  const Component = variantComponents[variant];
  return (
    <Component
      {...rest}
      class={`${variantStyles[variant]} ${rest.class ?? ""}`}
    />
  );
}
