const name = "keyboardnav";
export const ATTR_FOCUSABLE = `data-${name}-focusable`;
export const ATTR_GRID_ROW = `data-${name}-grid-row`;
export const ATTR_GROUP = `data-${name}-group`;

export const SEL_FOCUSABLE = `[${ATTR_FOCUSABLE}]`;
export const SEL_GRID_ROW = `[${ATTR_GRID_ROW}]`;
export const SEL_GROUP = `[${ATTR_GROUP}]`;

export const DATA_GROUP_TYPE = `${name}Group`;
export const DATA_TEMPORARY_FOCUSABLE = `${name}Temp`;

export const GROUP_TYPE_GRID = "grid";
export const GROUP_TYPE_HORIZONTAL = "horizontal";
export const GROUP_TYPE_VERTICAL = "vertical";

export const MOVE_DOWN = "d";
export const MOVE_UP = "u";
export const MOVE_LEFT = "l";
export const MOVE_RIGHT = "r";
export const MOVE_PREV = "p";
export const MOVE_NEXT = "n";
