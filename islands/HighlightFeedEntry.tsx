import { FunctionComponent } from "preact";
import { highlightItem } from "../state/operations.ts";
import { Feed, FeedItem } from "../state/State.ts";
import useAppState from "../state/useAppState.ts";
import Link from "./Link.tsx";

interface HighlightFeedEntryProps {
  feed: Feed;
  entry: FeedItem;
}

const HighlightFeedEntry: FunctionComponent<HighlightFeedEntryProps> = ({
  feed,
  entry,
}) => {
  const { dispatch } = useAppState();

  return (
    <div>
      <span class="opacity-80">
        <Link
          onClick={() =>
            dispatch(highlightItem, {
              type: "feed",
              feedId: feed.feedId,
            })}
        >
          {feed.name}
        </Link>{" "}
        &raquo;
      </span>{" "}
      {entry.title}
    </div>
  );
};

export default HighlightFeedEntry;
