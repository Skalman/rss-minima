import { FunctionComponent, JSX } from "preact";

interface AlertProps extends JSX.HTMLAttributes<HTMLDivElement> {
  variant: "error";
}

const Alert: FunctionComponent<AlertProps> = ({
  class: className,
  children,
  ...rest
}) => {
  return (
    <div
      class={`p-4 border border-red-800 text-red-400 bg-red-900 bg-opacity-50 rounded ${
        className ?? ""
      }`}
      {...rest}
    >
      {children}
    </div>
  );
};

export default Alert;
