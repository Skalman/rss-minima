import { Handler } from "$fresh/server.ts";
import { deleteSessionCookie } from "../lib/cookie.ts";

export const handler: Handler = (req, ctx) => {
  const headers = new Headers({
    location: new URL("./login", req.url).href,
  });

  deleteSessionCookie(headers);

  return new Response(undefined, { status: 302, headers });
};
