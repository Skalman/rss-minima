import { computed, signal } from "@preact/signals";
import memoComputed from "../lib/memoComputed.ts";
import State, { ListDisplay } from "./State.ts";

export default function createAppState(): State {
  const feeds: State["feeds"] = signal([]);

  const addFeed: State["addFeed"] = {
    submission: signal(undefined),
  };
  const removingFeed: State["removingFeed"] = signal(undefined);
  const refreshingFeeds: State["refreshingFeeds"] = signal([]);
  const fetchingFeeds: State["fetchingFeeds"] = signal(undefined);

  const listDisplay = signal<ListDisplay>({ feeds: "all", entries: "unread" });

  const sortTrueFirst = (a: boolean, b: boolean) => Number(b) - Number(a);

  const visibleFeeds: State["visibleFeeds"] = computed(() => {
    let result = feeds.value;

    if (listDisplay.value.feeds === "all") {
      result = result.slice();
    } else {
      result = result.filter((x) => x.items.some((x) => !x.isRead));
    }

    result.sort(
      (a, b) =>
        sortTrueFirst(a.enabled, b.enabled) ||
        a.name.localeCompare(b.name, "en"),
    );

    return result;
  });

  const highlightedItemId: State["highlightedItemId"] = signal(undefined);
  const highlightedItem: State["highlightedItem"] = computed(() => {
    const highlighted = highlightedItemId.value;
    if (!highlighted) return;

    const { type, feedId, feedItemId } = highlighted;

    if (type === "addFeed") {
      return { type };
    }

    const feed = feeds.value.find((x) => x.feedId === feedId);
    if (!feed) return;

    if (type === "feed") {
      return { type, feed };
    }

    const feedItem = feed.items.find((x) => x.feedItemId === feedItemId);
    if (!feedItem) return;

    return { type, feed, feedItem };
  });
  const highlightedShowAllEntries: State["highlightedShowAllEntries"] = signal(
    false,
  );

  const minute = 1000 * 60;
  function getNextFeedRefreshTimestamp(feed: State["feeds"]["value"][number]) {
    if (!feed.enabled) {
      return Infinity;
    }

    return (
      new Date(feed.lastErrorDateTime ?? feed.lastUpdatedDateTime).getTime() +
      feed.updateFrequencyMinutes * minute
    );
  }
  const nextFeedRefresh: State["nextFeedRefresh"] = memoComputed(
    () => {
      if (feeds.value.length === 0) {
        return undefined;
      }

      const feed = feeds.value.reduce((a, b) => {
        return getNextFeedRefreshTimestamp(a) <= getNextFeedRefreshTimestamp(b)
          ? a
          : b;
      });
      const refreshTimestamp = getNextFeedRefreshTimestamp(feed);

      return { feed, refreshTimestamp };
    },
    (a, b) =>
      a?.feed === b?.feed && a?.refreshTimestamp === b?.refreshTimestamp,
  );

  const state: State = {
    feeds,
    addFeed,
    removingFeed,
    refreshingFeeds,
    fetchingFeeds,
    listDisplay,
    visibleFeeds,
    highlightedItemId,
    highlightedItem,
    highlightedShowAllEntries,
    nextFeedRefresh,

    dispatch(fn, ...args: unknown[]) {
      return fn(state, ...args);
    },
  };

  return state;
}
