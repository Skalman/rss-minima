import { IS_BROWSER } from "$fresh/runtime.ts";
import { FunctionComponent } from "preact";
import { useEffect, useLayoutEffect, useState } from "preact/hooks";
import { effect } from "@preact/signals";
import { UserFeedDomain } from "../lib/db/userFeed.ts";
import appStateContext from "../state/appStateContext.ts";
import createAppState from "../state/createAppState.ts";
import {
  loadSettings,
  saveSettings,
  setInitialFeeds,
} from "../state/operations.ts";
import App from "./App.tsx";
import { useKeyboardNav } from "../lib/keyboard-nav/preact.ts";

interface RootProps {
  feeds: UserFeedDomain[];
  settings?: string;
}

const Root: FunctionComponent<RootProps> = function ({ feeds, settings }) {
  useKeyboardNav();
  const [state] = useState(createAppState());

  const [hasLoaded, setHasLoaded] = useState(false);

  if (!IS_BROWSER || !hasLoaded) {
    state.dispatch(setInitialFeeds, feeds);
    state.dispatch(loadSettings, settings);
  }

  useLayoutEffect(() => {
    setHasLoaded(true);
  }, []);

  useEffect(() => {
    const unsubscribe = effect(() => state.dispatch(saveSettings));
    return () => {
      unsubscribe();
    };
  }, [state]);

  return (
    <div class="h-full">
      <appStateContext.Provider value={state}>
        <App />
      </appStateContext.Provider>
    </div>
  );
};

export default Root;
