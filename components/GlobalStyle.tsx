import { FunctionComponent } from "preact";

const css = (css: TemplateStringsArray) => css[0].replace(/\s{2,}/g, " ");

const style = css`
  html,
  body {
    height: 100%;
    background: #000;
    color: #ddd;
  }
`;

const GlobalStyle: FunctionComponent = () => {
  return <style>{style}</style>;
};

export default GlobalStyle;
