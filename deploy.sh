#!/bin/bash

# Abort the script if a command returns an error.
set -e

deno task build


if [ -f .env.deploy ]; then
  # Export variables from .env.deploy file.
  set -o allexport
  source .env.deploy
  set +o allexport
fi


# Copy the content to a temporary directory.
rsync -avz --delete --quiet \
  --delete-excluded \
  --exclude={.git,.gitignore,deploy,deploy.sh,.env.example,.env.deploy,.env.deploy.example,.vscode} \
  -e ssh ./ ./deploy/

# Set version.
date --iso-8601=seconds > ./deploy/static/version.txt

# Generate the configuration file.
cat <<ENV > ./deploy/.env
MINIMA_SQLITE_DATABASE=$MINIMA_SQLITE_DATABASE
MINIMA_PORT=$MINIMA_PORT
MINIMA_AUTOMATIC_MIGRATIONS=$MINIMA_AUTOMATIC_MIGRATIONS
DENO_DEPLOYMENT_ID=$(git rev-parse HEAD)
ENV


# Transfer the content to the remote directory.
rsync -avz --delete -e ssh ./deploy/ "$USERNAME@$REMOTE_HOST:$REMOTE_DIR"
