import { DB } from "sqlite";
import migrations from "./migrations/mod.ts";
import { automaticMigrations, sqliteDatabase } from "../../env.ts";

Deno.mkdirSync("data", { recursive: true });
const database = new DB(sqliteDatabase);

let existingMigrationIds: string[];
try {
  existingMigrationIds = database
    .query<[string]>(`SELECT migrationId FROM migration`)
    .map(([migrationId]) => migrationId);
} catch (_error) {
  console.log("Initialize database");
  existingMigrationIds = [];

  const sql = `
    CREATE TABLE migration (
      migrationId TEXT PRIMARY KEY,
      createdDateTime TEXT
    ) STRICT;`;
  database.execute(sql);
}

const migrationsToRun = migrations.filter(
  ({ id }) => !existingMigrationIds.includes(id),
);

if (migrationsToRun.length) {
  console.log({ migrationsToRun: migrationsToRun.map(({ id }) => id) });
  if (automaticMigrations || confirm("Run migrations now?")) {
    database.transaction(() => {
      for (const { id, sql } of migrationsToRun) {
        try {
          database.execute(sql);
          database
            .prepareQuery(
              `INSERT INTO migration (migrationId, createdDateTime) VALUES (:migrationId, :createdDateTime)`,
            )
            .execute({
              migrationId: id,
              createdDateTime: new Date().toISOString(),
            });
        } catch (error) {
          console.error(`Failed to run migration '${id}'`);
          throw error;
        }
      }
    });
  } else {
    throw new Error("Cannot start app without running migrations");
  }
}

export default database;
