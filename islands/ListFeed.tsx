import { IS_BROWSER } from "$fresh/runtime.ts";
import { FunctionComponent } from "preact";
import { forwardRef, memo } from "preact/compat";
import {
  highlightItem,
  markFeedAsRead,
  markFeedAsUnread,
  setFeedEnabled,
  setFeedExpanded,
} from "../state/operations.ts";
import useAppState from "../state/useAppState.ts";
import IconCircle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle.tsx";
import IconCircleDot from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle-dot.tsx";
import IconButton from "./IconButton.tsx";
import Link from "./Link.tsx";
import IconAlertTriangle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/alert-triangle.tsx";
import IconChevronRight from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/chevron-right.tsx";
import IconChevronDown from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/chevron-down.tsx";
import IconEyeOff from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/eye-off.tsx";
import ListFeedEntry from "./ListFeedEntry.tsx";
import { Feed, FeedItem } from "../state/State.ts";
import useTooltip from "../lib/useTooltip.tsx";
import { useContext } from "preact/hooks";
import listDisplayContext from "../lib/listDisplayContext.ts";
import useCombinedRef from "../lib/useCombinedRef.ts";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

interface ListFeedProps {
  feed: Feed;
}

const EnableButton = forwardRef<HTMLButtonElement, { onClick(): void }>(
  ({ onClick }, ref) => {
    const t = useTooltip("Enable feed");
    const combinedRef = useCombinedRef(ref, t.ref);

    return (
      <>
        {t.node}
        <IconButton ref={combinedRef} icon={IconEyeOff} onClick={onClick} />
      </>
    );
  },
);

const ExpandButton = forwardRef<HTMLButtonElement, {
  expanded: boolean;
  dimmed: boolean;
  tooltip?: string;
  onClick(): void;
}>(({ expanded, dimmed, tooltip, onClick }, ref) => {
  const t = useTooltip(tooltip);
  const combinedRef = useCombinedRef(ref, t.ref);

  return (
    <>
      {t.node}
      <IconButton
        ref={combinedRef}
        icon={expanded ? IconChevronDown : IconChevronRight}
        onClick={onClick}
        class={dimmed ? "opacity-60" : undefined}
      />
    </>
  );
});

const ToggleReadButton = forwardRef<HTMLButtonElement, {
  hasUnread: boolean;
  disabled: boolean;
  tooltip?: string;
  onClick(): void;
}>(({ hasUnread, disabled, tooltip, onClick }, ref) => {
  const t = useTooltip(tooltip);
  const combinedRef = useCombinedRef(ref, t.ref);

  return (
    <>
      {t.node}
      <IconButton
        ref={combinedRef}
        icon={hasUnread ? IconCircleDot : IconCircle}
        onClick={onClick}
        disabled={disabled}
      />
    </>
  );
});

const DetailsButton = forwardRef<HTMLAnchorElement, {
  tooltip: string;
  error: boolean;
  name: string;
  unreadCount?: number;
  bold: boolean;
  onClick(): void;
}>(({ tooltip, error, name, unreadCount, bold, onClick }, ref) => {
  const t = useTooltip(tooltip);

  return (
    <>
      {t.node}
      <div
        ref={t.ref}
        class="flex-shrink overflow-hidden whitespace-nowrap overflow-ellipsis"
      >
        <Link
          ref={ref}
          onClick={onClick}
          class={bold ? "font-bold" : undefined}
        >
          {
            // A bug in Fresh causes an error when this is rendered server-side.
            IS_BROWSER && error && (
              <IconAlertTriangle
                size={"1em" as unknown as number}
                class="inline-block mr-2 text-yellow-400"
              />
            )
          }
          {name}
          {unreadCount !== undefined && ` (${unreadCount})`}
        </Link>
      </div>
    </>
  );
});

const EntriesList: FunctionComponent<{
  feed: Feed;
  text?: string;
  entries?: FeedItem[];
  showMoreText?: string;
  onShowMore?(): void;
}> = ({ feed, text, entries, showMoreText, onShowMore }) => {
  return (
    <>
      {text && <p class="italic opacity-50 pl-4 mb-1">{text}</p>}

      {entries && feed.feedId !== undefined && (
        <ul class="mb-2">
          {entries.map((x) => (
            <li key={x.feedItemId} class="ml-8" {...keyboardProps.gridRow}>
              <ListFeedEntry
                feed={feed}
                feedEntry={x}
              />
            </li>
          ))}
          {showMoreText && (
            <li class="ml-[5.5rem] italic" {...keyboardProps.gridRow}>
              <Link onClick={onShowMore}>
                {showMoreText}
              </Link>
            </li>
          )}
        </ul>
      )}
    </>
  );
};

const ListFeed: FunctionComponent<ListFeedProps> = ({ feed }) => {
  const { dispatch } = useAppState();

  let { feedId, expanded, enabled, items, name, lastError } = feed;

  const count = items.length;
  const readCount = items.reduce((sum, a) => sum + (a.isRead ? 1 : 0), 0);
  const unreadCount = count - readCount;

  const { feeds: displayFeeds, entries: displayEntries } = useContext(
    listDisplayContext,
  );

  if (displayEntries === "unread") {
    items = items.filter(({ isRead }) => !isRead);
  }

  const isListTruncated = items.length > 10;
  if (isListTruncated) {
    items = items.slice(0, 10);
  }

  const expandShowsNothing = displayEntries === "unread" && items.length === 0;

  const enableButton = !enabled && (
    <EnableButton
      onClick={() => {
        dispatch(setFeedEnabled, feedId, !enabled);
        dispatch(highlightItem, { type: "feed", feedId }, { force: true });
      }}
    />
  );

  const expandButton = enabled && (
    <ExpandButton
      expanded={expanded}
      dimmed={expandShowsNothing}
      tooltip={expandShowsNothing ? "There are no unread entries" : undefined}
      onClick={() => dispatch(setFeedExpanded, feedId, !expanded)}
    />
  );

  const toggleReadButton = enabled && (
    <ToggleReadButton
      hasUnread={unreadCount > 0}
      disabled={count === 0}
      tooltip={unreadCount > 0
        ? `Mark ${unreadCount} as read`
        : readCount > 0
        ? `Mark ${readCount} as unread`
        : undefined}
      onClick={() =>
        dispatch(unreadCount > 0 ? markFeedAsRead : markFeedAsUnread, feedId)}
    />
  );

  const detailsButton = (
    <DetailsButton
      tooltip={lastError
        ? "Show feed details and last fetch error"
        : "Show feed details"}
      error={!!lastError}
      name={name}
      unreadCount={!enabled || (expanded && !expandShowsNothing)
        ? undefined
        : unreadCount}
      bold={enabled && unreadCount > 0 && displayFeeds === "all"}
      onClick={() => dispatch(highlightItem, { type: "feed", feedId })}
    />
  );

  const entriesList = enabled && expanded && (
    <EntriesList
      feed={feed}
      text={items.length === 0 && displayEntries === "all"
        ? "No entries"
        : undefined}
      entries={items.length !== 0 ? items : undefined}
      showMoreText={isListTruncated ? `Show all ${count} entries` : undefined}
      onShowMore={isListTruncated
        ? () => {
          dispatch(
            highlightItem,
            { type: "feed", feedId },
            { showAllEntries: true },
          );
        }
        : undefined}
    />
  );

  return (
    <>
      <div
        class={`flex gap-2 ${enabled ? "" : "opacity-60"}`}
        {...keyboardProps.gridRow}
      >
        <div class="whitespace-pre">
          {enableButton}
          {expandButton}
          {toggleReadButton}
        </div>

        {detailsButton}
      </div>
      {entriesList}
    </>
  );
};

export default memo(ListFeed);
