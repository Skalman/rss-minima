export default {
  id: "2_add_userFeed.sort",
  sql: `
    ALTER TABLE userFeed
    ADD COLUMN sort INTEGER NULL;
    `,
};
