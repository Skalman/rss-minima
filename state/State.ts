import { ReadonlySignal, Signal } from "@preact/signals";
import { CopyFormat } from "../lib/db/userFeed.ts";

export interface FeedItem {
  feedItemId: number;
  title: string;
  url: string;
  isRead: boolean;
  content?: string;
  attachmentUrl?: string;
  dateTime: string;
}

export interface Feed {
  feedId: number;
  url: string;
  name: string;
  originalName: string;
  expanded: boolean;
  enabled: boolean;
  updateFrequencyMinutes: number;
  copyFormat?: CopyFormat;
  addedDateTime: string;
  lastUpdatedDateTime: string;
  lastError?: string;
  lastErrorDateTime?: string;
  items: FeedItem[];
}

type AsyncOperation<
  Payload extends Record<string, unknown> = Record<never, never>,
> =
  | undefined
  | ({ state: "in-progress" } & Payload)
  | { state: "error"; error: unknown };

export type HighlightedItemId =
  | { type: "feed"; feedId: number; feedItemId?: undefined }
  | { type: "feedItem"; feedId: number; feedItemId: number }
  | { type: "addFeed"; feedId?: undefined; feedItemId?: undefined };

type HighlightedItem =
  | { type: "feed"; feed: Feed; feedItem?: undefined }
  | { type: "feedItem"; feed: Feed; feedItem: FeedItem }
  | { type: "addFeed"; feed?: undefined; feedItem?: undefined };

interface NextFeedRefresh {
  feed: Feed;
  refreshTimestamp: number;
}

export type ListDisplay =
  | { feeds: "all"; entries: "all" }
  | { feeds: "all"; entries: "unread" }
  | { feeds: "unread"; entries: "unread" };

export default interface State {
  feeds: Signal<Feed[]>;
  addFeed: {
    submission: Signal<AsyncOperation<{ url: string }>>;
  };
  removingFeed: Signal<AsyncOperation<{ feedId: number }>>;
  refreshingFeeds: Signal<number[]>;
  fetchingFeeds: Signal<AsyncOperation>;
  listDisplay: Signal<ListDisplay>;
  visibleFeeds: ReadonlySignal<Feed[]>;
  highlightedItemId: Signal<HighlightedItemId | undefined>;
  highlightedItem: ReadonlySignal<HighlightedItem | undefined>;
  highlightedShowAllEntries: Signal<boolean>;
  nextFeedRefresh: ReadonlySignal<NextFeedRefresh | undefined>;

  // deno-lint-ignore no-explicit-any
  dispatch<T extends (state: State, ...args: any[]) => any>(
    fn: T,
    ...args: ParametersExceptFirst<T>
  ): ReturnType<T>;
}

type ParametersExceptFirst<
  // deno-lint-ignore no-explicit-any
  T extends (arg: any, ...args: any) => unknown,
> // deno-lint-ignore no-explicit-any
 = T extends (arg: any, ...args: infer P) => unknown ? P : never;
