import { Handler } from "$fresh/server.ts";
import BackEndApi from "../lib/BackEndApi.ts";
import { getSessionFromCookie } from "../lib/cookie.ts";

export const handler: Record<string, Handler> = {
  async POST(req) {
    const sessionId = getSessionFromCookie(req.headers);
    if (!sessionId) {
      return new Response(
        JSON.stringify({ error: { message: "Not logged in" } }),
        { status: 401 },
      );
    }

    const [operation, ...args] = await req.json();
    try {
      // deno-lint-ignore no-explicit-any
      const api = new BackEndApi(sessionId) as any;
      const result = await api[operation](...args);
      let resultInspect = Deno.inspect(result, { colors: !Deno.noColor });
      if (resultInspect.length > 200) {
        resultInspect = resultInspect.slice(0, 200) + "\x1b[39m...\n";
      }
      console.log(resultInspect, { operation, args });
      return new Response(JSON.stringify({ result }));
    } catch (error) {
      console.error(error);
      return new Response(
        JSON.stringify({
          error: error instanceof Error
            ? {
              name: error.name,
              message: error.message,
              cause: error.cause,
              stack: error.stack,
            }
            : error,
        }),
        {
          status: 500,
        },
      );
    }
  },
};
