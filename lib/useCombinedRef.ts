import { useMemo } from "preact/compat";
import { Ref } from "preact";

const useCombinedRef = <T>(a: Ref<T>, b: Ref<T>): Ref<T> => {
  return useMemo(() => {
    if (!a || !b) {
      return a ?? b;
    }

    return (x: T | null) => {
      if (typeof a === "function") a(x);
      else a.current = x;

      if (typeof b === "function") b(x);
      else b.current = x;
    };
  }, [a, b]);
};

export default useCombinedRef;
