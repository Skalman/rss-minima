export default {
  id: "9_add_feed.lastError_and_feed.lastErrorDateTime",
  sql: `
    ALTER TABLE feed
    ADD COLUMN lastError TEXT NULL;

    ALTER TABLE feed
    ADD COLUMN lastErrorDateTime TEXT NULL;
  `,
};
