import { FunctionComponent, Ref } from "preact";
import IconCopy from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/copy.tsx";
import { useSignal } from "@preact/signals";
import IconButton from "./IconButton.tsx";
import useTooltip from "../lib/useTooltip.tsx";
import { forwardRef, useMemo } from "preact/compat";

export interface CopyButtonProps {
  title: string;
  text: string;
}

const CopyButton: FunctionComponent<CopyButtonProps> = forwardRef<
  HTMLButtonElement,
  CopyButtonProps
>(function ({
  title,
  text,
}, ref) {
  const showTooltip = useSignal<true | undefined>(undefined);
  const copyMessageTimeout = useSignal<number | undefined>(undefined);
  const didCopy = copyMessageTimeout.value !== undefined;
  showTooltip.value = didCopy || undefined;
  const tooltip = useTooltip(didCopy ? "Copied" : title, showTooltip);

  const combinedRef = useMemo((): Ref<HTMLButtonElement> => {
    if (!ref) {
      return tooltip.ref;
    }

    return (x) => {
      tooltip.ref(x);
      if (typeof ref === "function") {
        ref(x);
      } else {
        ref.current = x;
      }
    };
  }, [tooltip.ref, ref]);

  return (
    <>
      {tooltip.node}
      <IconButton
        ref={combinedRef}
        icon={IconCopy}
        onClick={() => {
          navigator.clipboard.writeText(text);

          clearTimeout(copyMessageTimeout.value);
          copyMessageTimeout.value = setTimeout(() => {
            copyMessageTimeout.value = undefined;
          }, 1000);
        }}
        active={didCopy}
      />
    </>
  );
});

export default CopyButton;
