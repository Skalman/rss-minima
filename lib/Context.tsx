import { compare, Dir, eq, NavItem } from "./useKeyboardNav.tsx";

export class Context {
  private #all: Map<string, NavItem> = new Map();
  private sorted?: NavItem[];

  get(key: string) {
    return this.#all.get(key);
  }

  set(key: string, navItem: NavItem) {
    this.sorted = undefined;
    return this.#all.set(key, navItem);
  }

  delete(key: string) {
    this.sorted = undefined;
    return this.#all.delete(key);
  }

  find(
    origin: { x: number[]; y: number[] },
    dir: Dir,
  ) {
    if (!this.sorted) {
      this.sorted = Array.from(this.#all.values()).sort((a, b) =>
        compare(a.x, b.x) || compare(a.y, b.y)
      );
    }

    if (dir === "up") {
      const yMatch = this.sorted.findLast((navItem) =>
        compare(navItem.y, origin.y) < 0
      );
      const xMatch = yMatch &&
        this.sorted.findLast((navItem) =>
          eq(navItem.y, yMatch.y) && compare(navItem.x, origin.x) <= 0
        );
      console.log("up", { origin, yMatch, xMatch });
      return xMatch;
    }

    if (dir === "down") {
      const yMatch = this.sorted.find((navItem) =>
        compare(navItem.y, origin.y) > 0
      );
      const xMatch = yMatch &&
        this.sorted.findLast((navItem) =>
          eq(navItem.y, yMatch.y) && compare(navItem.x, origin.x) <= 0
        );
      return xMatch;
    }

    if (dir === "left") {
      const match = this.sorted.findLast((navItem) =>
        eq(navItem.y, origin.y) && compare(navItem.x, origin.x) < 0
      );
      return match;
    }

    if (dir === "right") {
      const match = this.sorted.find((navItem) =>
        eq(navItem.y, origin.y) && compare(navItem.x, origin.x) > 0
      );
      return match;
    }
  }
}
