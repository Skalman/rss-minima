import BackEndApi from "./BackEndApi.ts";

// deno-lint-ignore no-explicit-any
type AsyncFunc<T extends (...args: any) => any> = T extends (
  ...args: infer A
) => infer R ? (...args: A) => Promise<Awaited<R>>
  : never;

export type AsyncApi = {
  [P in keyof BackEndApi]: AsyncFunc<BackEndApi[P]>;
};

const getFrontEndApi = function () {
  return new Proxy(
    {},
    {
      get: (_target, operationName) => async (...args: unknown[]) => {
        const response = await fetch("./api", {
          method: "post",
          body: JSON.stringify([operationName, ...args]),
        });
        const json = await response.json();

        if (json.error) {
          throw Object.assign(new Error(), json.error);
        }

        return json.result;
      },
    },
  ) as AsyncApi;
};

export default getFrontEndApi;
