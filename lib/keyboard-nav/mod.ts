import {
  DATA_GROUP_TYPE,
  DATA_TEMPORARY_FOCUSABLE,
  GROUP_TYPE_GRID,
  GROUP_TYPE_HORIZONTAL,
  GROUP_TYPE_VERTICAL,
  MOVE_DOWN,
  MOVE_LEFT,
  MOVE_NEXT,
  MOVE_PREV,
  MOVE_RIGHT,
  MOVE_UP,
  SEL_FOCUSABLE,
  SEL_GRID_ROW,
  SEL_GROUP,
} from "./constants.ts";
import {
  arrayAt,
  findAll,
  findAllFocusable,
  findAllVisible,
  isElement,
  isElementFocusable,
  Lazy,
} from "./util.ts";

type GridDirection =
  | typeof MOVE_UP
  | typeof MOVE_DOWN
  | typeof MOVE_LEFT
  | typeof MOVE_RIGHT;
type SequenceDirection = typeof MOVE_PREV | typeof MOVE_NEXT;

interface GroupModel {
  readonly type: string;
  readonly container: HTMLElement;

  /** Returns true if it was possible to set the given element as current. */
  setCurrent(focusableElement: HTMLElement): boolean;

  /** Returns the element that was focused, if any. */
  focusCurrent(): HTMLElement | undefined;

  /** Returns true if the focus was moved. */
  moveFocus(direction: GridDirection): boolean;
}

interface GridCurrent {
  cell: HTMLElement;
  row: HTMLElement;
  x: number;
  y: number;
  requestedX?: number;
  rows: HTMLElement[];
  cells: HTMLElement[];
}
class GridModel implements GroupModel {
  readonly #wraparound;
  #current?: GridCurrent;
  readonly type = GROUP_TYPE_GRID;

  constructor(readonly container: HTMLElement, wraparound: boolean) {
    this.#wraparound = wraparound;
  }

  setCurrent(focusableElement: HTMLElement): boolean {
    if (!isElementFocusable(focusableElement)) return false;

    if (!focusableElement.matches(SEL_FOCUSABLE)) return false;

    const container = this.container;
    if (!container.contains(focusableElement)) return false;

    const row = focusableElement.closest(SEL_GRID_ROW);
    if (!isElement(row)) return false;

    const rows = findAllVisible(container, SEL_GRID_ROW);
    const cells = findAllFocusable(row, SEL_FOCUSABLE);

    this.#current = {
      cell: focusableElement,
      row: row,
      rows,
      cells,
      x: cells.indexOf(focusableElement),
      y: rows.indexOf(row),
      requestedX: this.#current?.cell === focusableElement
        ? this.#current.requestedX
        : undefined,
    };
    return true;
  }

  #getCurrent(): GridCurrent | undefined {
    const container = this.container;
    let { cell, row, x, y, requestedX } = this.#current ??
      { x: 0, y: 0 };

    const rows = findAllVisible(container, SEL_GRID_ROW);

    if (!row || !rows.includes(row)) {
      row = rows[y] ?? rows.at(-1);
    }

    if (!row) return;

    const cells = findAllFocusable(row, SEL_FOCUSABLE);

    if (!cell || !cells.includes(cell)) {
      cell = cells[requestedX ?? x] ?? rows.at(-1);
    }

    if (!cell) return;

    return {
      cell,
      row,
      rows,
      cells,
      x: cells.indexOf(cell),
      y: rows.indexOf(row),
      requestedX,
    };
  }

  focusCurrent(): HTMLElement | undefined {
    this.#current = this.#getCurrent();
    this.#current?.cell.focus();
    return this.#current?.cell;
  }

  #tryMoveFocusUpDown(direction: -1 | 1): boolean {
    const current = this.#getCurrent();
    if (!current) return false;

    const row = arrayAt(current.rows, current.y + direction, this.#wraparound);
    if (!row) return false;

    const requestedX = current.requestedX ?? current.x;
    const cells = findAllFocusable(row, SEL_FOCUSABLE);
    const cell = cells[requestedX] ?? cells.at(-1);
    if (!cell) return false;

    current.y += direction;
    current.row = row;
    current.cells = cells;
    current.cell = cell;
    current.x = cells.indexOf(cell);
    current.requestedX = requestedX;
    this.#current = current;
    cell.focus();
    return true;
  }

  #tryMoveFocusLeftRight(direction: -1 | 1): boolean {
    const current = this.#getCurrent();
    if (!current) return false;

    const cell = arrayAt(
      current.cells,
      current.x + direction,
      this.#wraparound,
    );
    if (!cell) return false;

    current.cell = cell;
    current.x = current.cells.indexOf(cell);
    current.requestedX = undefined;
    this.#current = current;
    cell.focus();
    return true;
  }

  moveFocus(direction: GridDirection): boolean {
    if (direction === MOVE_UP) {
      return this.#tryMoveFocusUpDown(-1);
    } else if (direction === MOVE_DOWN) {
      return this.#tryMoveFocusUpDown(1);
    } else if (direction === MOVE_LEFT) {
      return this.#tryMoveFocusLeftRight(-1);
    } else if (direction === MOVE_RIGHT) {
      return this.#tryMoveFocusLeftRight(1);
    }

    return false;
  }
}

interface SequenceCurrent {
  cell: HTMLElement;
  cells: HTMLElement[];
  index: number;
}
class SequenceModel implements GroupModel {
  readonly #wraparound;
  #current?: SequenceCurrent;
  constructor(
    readonly type: typeof GROUP_TYPE_HORIZONTAL | typeof GROUP_TYPE_VERTICAL,
    readonly container: HTMLElement,
    wraparound: boolean,
  ) {
    this.#wraparound = wraparound;
  }

  setCurrent(focusableElement: HTMLElement): boolean {
    if (!isElementFocusable(focusableElement)) return false;

    if (!focusableElement.matches(SEL_FOCUSABLE)) return false;

    const container = this.container;
    if (!container.contains(focusableElement)) return false;

    const cells = findAllFocusable(container, SEL_FOCUSABLE);

    this.#current = {
      cell: focusableElement,
      cells,
      index: cells.indexOf(focusableElement),
    };
    return true;
  }

  #getCurrent(): SequenceCurrent | undefined {
    const container = this.container;
    let { cell, index } = this.#current ?? { index: 0 };

    const cells = findAllFocusable(container, SEL_FOCUSABLE);

    if (!cell || !cells.includes(cell)) {
      cell = cells[index] ?? cells.at(-1);
    }

    if (!cell) return;

    return { cell, cells, index: cells.indexOf(cell) };
  }

  focusCurrent(): HTMLElement | undefined {
    this.#current = this.#getCurrent();
    this.#current?.cell.focus();
    return this.#current?.cell;
  }

  #tryMoveFocus(direction: -1 | 1): boolean {
    const current = this.#getCurrent();
    if (!current) return false;

    const cell = arrayAt(
      current.cells,
      current.index + direction,
      this.#wraparound,
    );
    if (!cell) return false;

    current.cell = cell;
    current.index = current.cells.indexOf(cell);
    this.#current = current;
    cell.focus();
    return true;
  }

  moveFocus(direction: GridDirection): boolean {
    const type = this.type;
    if (
      (direction === MOVE_LEFT && type == GROUP_TYPE_HORIZONTAL) ||
      (direction === MOVE_UP && type === GROUP_TYPE_VERTICAL)
    ) {
      return this.#tryMoveFocus(-1);
    } else if (
      (direction === MOVE_RIGHT && type == GROUP_TYPE_HORIZONTAL) ||
      (direction === MOVE_DOWN && type === GROUP_TYPE_VERTICAL)
    ) {
      return this.#tryMoveFocus(1);
    }

    return false;
  }
}

class DocumentModel {
  readonly #groups = new WeakMap<Element, GroupModel>();
  readonly #tabWraparound;
  readonly #creators: Record<
    string,
    ((container: HTMLElement) => GroupModel) | undefined
  >;

  constructor(
    { tabWraparound = false, arrowKeyWraparound = true }: KeyboardNavOptions =
      {},
  ) {
    this.#tabWraparound = tabWraparound;

    this.#creators = {
      [GROUP_TYPE_GRID]: (x) => new GridModel(x, arrowKeyWraparound),

      [GROUP_TYPE_HORIZONTAL]: (x) =>
        new SequenceModel(GROUP_TYPE_HORIZONTAL, x, arrowKeyWraparound),

      [GROUP_TYPE_VERTICAL]: (x) =>
        new SequenceModel(GROUP_TYPE_VERTICAL, x, arrowKeyWraparound),
    };
  }

  getGroupModel(element: HTMLElement): GroupModel | undefined {
    const groupElement = element.closest(SEL_GROUP);
    if (!isElement(groupElement)) return;

    const groupType = groupElement.dataset[DATA_GROUP_TYPE];
    let groupModel = this.#groups.get(groupElement);
    if (!groupModel || groupModel.type !== groupType) {
      groupModel = groupType
        ? this.#creators[groupType]?.(groupElement)
        : undefined;

      if (groupModel) {
        this.#groups.set(groupElement, groupModel);
      } else {
        this.#groups.delete(groupElement);
      }
    }

    return groupModel;
  }

  getAdjacentGroupModel(
    groupModel: GroupModel,
    direction: SequenceDirection,
  ): GroupModel | undefined {
    const groupElements = findAll(document.body, SEL_GROUP);
    const index = groupElements.indexOf(groupModel.container);
    if (index === -1) return;

    const dir = direction === MOVE_PREV ? -1 : 1;
    const adjacentElement = arrayAt(
      groupElements,
      index + dir,
      this.#tabWraparound,
    );

    return adjacentElement && this.getGroupModel(adjacentElement);
  }
}

export interface KeyboardNavOptions {
  /**
   * Whether to
   * Default: `false`
   */
  tabWraparound?: boolean;

  /**
   * Whether
   * Default: `true`
   */
  arrowKeyWraparound?: boolean;
}

export class KeyboardNav {
  readonly #documentModel;

  constructor(options?: KeyboardNavOptions) {
    this.#documentModel = new DocumentModel(options);
  }

  enable() {
    document.addEventListener("keydown", this.#keydownListener);
    document.addEventListener("focus", this.#focusListener, { capture: true });
  }

  disable() {
    document.removeEventListener("keydown", this.#keydownListener);
    document.removeEventListener("focus", this.#focusListener);
    this.#subTreeObserver.valueIfInitialized?.disconnect();
    this.#temporaryFocusableDiv.valueIfInitialized?.remove();
  }

  readonly #subTreeObserver = new Lazy(() =>
    new MutationObserver(() => {
      if (this.#lastFocused && !isElementFocusable(this.#lastFocused.element)) {
        const newElement = this.#lastFocused.group.focusCurrent();
        if (newElement) {
          this.#lastFocused = {
            group: this.#lastFocused.group,
            element: newElement,
          };
        }
      }
    })
  );

  readonly #temporaryFocusableDiv = new Lazy(() => {
    const div = document.createElement("div");
    div.tabIndex = 0;
    div.dataset[DATA_TEMPORARY_FOCUSABLE] = "";
    Object.assign(div.style, {
      position: "absolute",
      width: 0,
      height: 0,
      overflow: "hidden",
    });
    div.onblur = () => div.remove();
    return div;
  });

  #lastFocused?: {
    group: GroupModel;
    element: HTMLElement;
  };

  #focusListener = (e: FocusEvent) => {
    this.#subTreeObserver.valueIfInitialized?.disconnect();

    const element = e.target;
    if (element instanceof HTMLElement) {
      const group = this.#documentModel.getGroupModel(element);
      if (group) {
        const didSet = group.setCurrent(element);
        if (didSet) {
          this.#lastFocused = { group, element };
          this.#subTreeObserver.value.observe(group.container, {
            childList: true,
            subtree: true,
            attributes: true,
          });
        }
      }
    }
  };

  static #keyDirectionMap: Record<string, GridDirection> = {
    ArrowDown: MOVE_DOWN,
    ArrowUp: MOVE_UP,
    ArrowLeft: MOVE_LEFT,
    ArrowRight: MOVE_RIGHT,
  };

  #keydownListener = (e: KeyboardEvent) => {
    const key = e.key;

    if (e.altKey || e.ctrlKey || e.metaKey) return;

    if (!(e.target instanceof HTMLElement)) return;

    let didFocus: boolean | undefined;

    if (key === "Tab") {
      const groupModel = this.#documentModel.getGroupModel(e.target);

      if (groupModel) {
        const adjacentModel = this.#documentModel.getAdjacentGroupModel(
          groupModel,
          e.shiftKey ? MOVE_PREV : MOVE_NEXT,
        );

        if (adjacentModel) {
          didFocus = !!adjacentModel.focusCurrent();
        } else {
          // We can't control focus fully, but we can ensure that tabbing twice
          // will allow the user to tab out of the page.
          const div = this.#temporaryFocusableDiv.value;
          if (e.shiftKey) {
            document.body.prepend(div);
          } else {
            document.body.append(div);
          }
          div.focus();
          didFocus = true;
        }
      }
    } else if (!e.shiftKey && KeyboardNav.#keyDirectionMap[key]) {
      didFocus = this.#documentModel.getGroupModel(e.target)?.moveFocus(
        KeyboardNav.#keyDirectionMap[key],
      );
    }

    if (didFocus) {
      e.preventDefault();
    }
  };
}
