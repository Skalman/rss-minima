import { Handler, PageProps } from "$fresh/server.ts";
import { Head } from "$fresh/runtime.ts";
import Alert from "../components/Alert.tsx";
import Button from "../islands/Button.tsx";
import TextField from "../islands/TextField.tsx";
import Typography from "../islands/Typography.tsx";
import { getSessionFromCookie, setSessionCookie } from "../lib/cookie.ts";
import { createUserSession } from "../lib/db/userSession.ts";
import Center from "../components/Center.tsx";
import GlobalStyle from "../components/GlobalStyle.tsx";

interface LoginProps {
  error?: "bad-password";
  username?: string;
}

export const handler: Record<string, Handler<LoginProps>> = {
  GET: (req, ctx) => {
    const sessionId = getSessionFromCookie(req.headers);
    if (sessionId) {
      return Response.redirect(new URL("./logout", req.url));
    }

    return ctx.render({});
  },

  POST: async (req, ctx) => {
    const formData = new URLSearchParams(await req.text());
    const username = formData.get("username");
    const password = formData.get("password");

    if (!username || !password) {
      return await ctx.render({});
    }

    const { result, sessionId } = createUserSession({
      usernameDisplay: username,
      password,
    });

    if (result === "created-user" || result === "logged-in") {
      const headers = new Headers({
        Location: new URL("./", req.url).href,
      });
      setSessionCookie(headers, sessionId);
      return new Response(undefined, { status: 302, headers });
    }
    console.log({ result, username });
    return ctx.render({ error: result, username });
  },
};

export default function Login({ data }: PageProps<LoginProps>) {
  const { error, username } = data;
  return (
    <>
      <Head>
        <title>RSS Minima</title>
        <GlobalStyle />
      </Head>
      <Center class="h-full">
        <form method="post">
          <div class="flex-grow" />
          <Typography variant="h1" class="mb-8">
            Log in or create an account
          </Typography>
          {error && (
            <Alert variant="error" class="mb-4">
              Bad password
            </Alert>
          )}
          <TextField
            label="Username"
            name="username"
            class="mb-4"
            value={username}
            required
          />
          <TextField
            label="Password"
            name="password"
            type="password"
            class="mb-4"
            required
          />

          <Button type="submit" variant="primary">
            Log in or create account
          </Button>
        </form>
      </Center>
      {" "}
    </>
  );
}
