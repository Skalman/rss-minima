import { FunctionComponent } from "preact";
import listDisplayContext from "../lib/listDisplayContext.ts";
import useAppState from "../state/useAppState.ts";
import ListFeed from "./ListFeed.tsx";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

const List: FunctionComponent = () => {
  const { visibleFeeds, listDisplay } = useAppState();
  return (
    <listDisplayContext.Provider value={listDisplay.value}>
      <ul {...keyboardProps.grid}>
        {visibleFeeds.value.map((feed, i) => (
          <li key={feed.feedId}>
            <ListFeed feed={feed} />
          </li>
        ))}
      </ul>
    </listDisplayContext.Provider>
  );
};

export default List;
