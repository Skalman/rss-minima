import { FunctionComponent } from "preact";
import useAppState from "../state/useAppState.ts";
import AddFeedForm from "./AddFeedForm.tsx";
import HighlightFeed from "./HighlightFeed.tsx";
import HighlightFeedEntry from "./HighlightFeedEntry.tsx";

const Highlight: FunctionComponent = function () {
  const { highlightedItem } = useAppState();
  const item = highlightedItem.value;

  if (!item) {
    return <div class="opacity-50 italic">Nothing selected</div>;
  }

  const { type, feed, feedItem } = item;
  if (type === "addFeed") {
    return <AddFeedForm />;
  }

  if (type === "feed") {
    return <HighlightFeed feed={feed} />;
  }

  return <HighlightFeedEntry feed={feed} entry={feedItem} />;
};

export default Highlight;
