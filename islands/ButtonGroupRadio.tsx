import Button from "./Button.tsx";

export interface ButtonGroupRadioProps<T> {
  value: T;
  options: { value: T; label?: string }[];
  onOptionChange: (value: T) => void;
}

const ButtonGroupRadio = function <T,>({
  value,
  options,
  onOptionChange,
}: ButtonGroupRadioProps<T>) {
  // Difficult to implement with real radio buttons before Twind 1.0.
  return (
    <span class="inline-block">
      {options.map((x, i) => (
        <Button
          key={x.value}
          variant={x.value === value ? "primary" : "tertiary"}
          rounded={i === 0
            ? "left"
            : i === options.length - 1
            ? "right"
            : "none"}
          class={i === 0 ? undefined : "-ml-px"}
          onClick={() => {
            onOptionChange(x.value);
          }}
        >
          {x.label ?? `${x.value}`}
        </Button>
      ))}
    </span>
  );
};

export default ButtonGroupRadio;
