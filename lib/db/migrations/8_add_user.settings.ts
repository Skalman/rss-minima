export default {
  id: "8_add_user.settings",
  sql: `
    ALTER TABLE user
    ADD COLUMN settings TEXT NULL;
    `,
};
