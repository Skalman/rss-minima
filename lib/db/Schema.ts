import { FeedSchema } from "./feed.ts";
import { FeedItem } from "./feedItem.ts";
import { UserSchema } from "./user.ts";
import { UserFeedSchema } from "./userFeed.ts";
import { UserFeedItemReadSchema } from "./userFeedItemRead.ts";
import { UserSessionSchema } from "./userSession.ts";

export default interface Schema {
  migration: {
    id: string;
    created: number;
  };

  user: UserSchema;

  userSession: UserSessionSchema;

  feed: FeedSchema;

  feedItem: FeedItem;

  userFeed: UserFeedSchema;

  userFeedItemRead: UserFeedItemReadSchema;
}
