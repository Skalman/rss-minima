import { deleteCookie, getCookies, setCookie } from "$std/http/cookie.ts";

const SESSION_COOKIE = "rssMinimaSession";

export function getSessionFromCookie(headers: Headers): string | undefined {
  return getCookies(headers)[SESSION_COOKIE];
}

export function setSessionCookie(headers: Headers, sessionId: string) {
  setCookie(headers, {
    name: SESSION_COOKIE,
    value: sessionId,
    httpOnly: true,
    maxAge: 60 * 60 * 24 * 30, // month
    sameSite: "Strict",
  });
}

export function deleteSessionCookie(headers: Headers) {
  deleteCookie(headers, SESSION_COOKIE);
}
