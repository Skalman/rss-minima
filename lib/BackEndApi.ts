import { Feed, parseFeed } from "rss/mod.ts";
import database from "./db/database.ts";
import {
  FeedSchema,
  insertFeed,
  selectFeedById,
  updateFeed,
} from "./db/feed.ts";
import { upsertFeedItems } from "./db/feedItem.ts";
import { updateUser } from "./db/user.ts";
import {
  CopyFormat,
  deleteUserFeed,
  getUserFeedByUrl,
  insertUserFeed,
  selectUserFeed,
  selectUserFeeds,
  updateUserFeed,
  UserFeedDomain,
} from "./db/userFeed.ts";
import {
  deleteUserFeedItemRead,
  insertUserFeedItemRead,
} from "./db/userFeedItemRead.ts";
import { selectUserIdBySessionId } from "./db/userSession.ts";

export default class BackEndApi {
  #userId;

  constructor(sessionId: string) {
    const userId = selectUserIdBySessionId(sessionId);
    if (!userId) {
      throw new Error("Invalid session");
    }
    this.#userId = userId;
  }

  getFeeds(): UserFeedDomain[] {
    return selectUserFeeds(this.#userId);
  }

  async #fetchFeed(url: string) {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(
        `Failed to fetch ${url}. Received ${response.status} ${response.statusText}.`,
      );
    }
    const xml = await response.text();
    const parsedFeed = await parseFeed(xml);
    return parsedFeed;
  }

  async subscribe(url: string) {
    let parsedFeed: Feed;
    try {
      parsedFeed = await this.#fetchFeed(url);
    } catch (e) {
      throw e;
    }

    const feedId = database.transaction(() => {
      const existingFeed = getUserFeedByUrl(this.#userId, url);
      let feedId = existingFeed?.feed.feedId;
      if (!feedId) {
        feedId = insertFeed({ url, name: "", lastUpdatedDateTime: "" });
      }

      if (!existingFeed?.userFeed) {
        insertUserFeed({
          userId: this.#userId,
          feedId,
          updateFrequencyMinutes: 60,
        });
      }

      return feedId;
    });

    const feed = selectFeedById(feedId);
    if (!feed) {
      throw new Error(`Invariant: Expected feed to exist`);
    }

    this.#upsertFeed(feed, parsedFeed);
    return this.#selectUserFeed(feed.feedId)!;
  }

  unsubscribe(feedId: number) {
    deleteUserFeed({ userId: this.#userId, feedId });
  }

  #upsertFeed(feed: FeedSchema, parsedFeed: Feed) {
    const now = new Date().toISOString();
    const feedItems = parsedFeed.entries
      .filter((x) => {
        const numViews = x["media:group"]?.["media:community"]
          ?.["media:statistics"]?.views;

        // If the entry hasn't had *any* views, it probably hasn't been published yet.
        if (numViews === "0") {
          return false;
        }

        return true;
      })
      .map(({ title, published, updated, links }) => ({
        title: title?.value ?? "[missing title]",
        feedItemDateTime: (published ?? updated)?.toISOString() ?? now,
        url: links.find((x) => x.type === undefined || x.type === "alternate")
          ?.href ?? "[missing url]",
      }));

    upsertFeedItems(feed.feedId, feedItems);

    updateFeed({
      feedId: feed.feedId,
      url: feed.url,
      name: parsedFeed.title.value ?? feed.name,
      lastUpdatedDateTime: now,
      lastError: null,
      lastErrorDateTime: null,
    });
  }

  #setFeedError(feedId: number, error: unknown) {
    updateFeed({
      feedId,
      lastError: `${error}`,
      lastErrorDateTime: new Date().toISOString(),
    });
  }

  #selectUserFeed(feedId: number) {
    return selectUserFeed({
      userId: this.#userId,
      feedId,
    })!;
  }

  async refreshFeed(feedId: number): Promise<UserFeedDomain> {
    const feed = selectFeedById(feedId);
    if (!feed) {
      throw new Error("Feed not found");
    }

    let parsedFeed: Feed;
    try {
      parsedFeed = await this.#fetchFeed(feed.url);
    } catch (e) {
      this.#setFeedError(feedId, e);
      return this.#selectUserFeed(feed.feedId)!;
    }

    this.#upsertFeed(feed, parsedFeed);
    return this.#selectUserFeed(feed.feedId)!;
  }

  markAs(mode: "read" | "unread", feedItemIds: number[]) {
    if (mode === "read") {
      insertUserFeedItemRead({ feedItemIds, userId: this.#userId });
    } else {
      deleteUserFeedItemRead({ feedItemIds, userId: this.#userId });
    }
  }

  setFeedExpanded(feedId: number, expanded: boolean) {
    updateUserFeed({ userId: this.#userId, feedId, expanded });
  }

  setFeedCustomName({
    feedId,
    customName,
  }: {
    feedId: number;
    customName?: string;
  }) {
    updateUserFeed({
      userId: this.#userId,
      feedId,
      customName: customName ?? null,
    });
  }

  setFeedUpdateFrequency(feedId: number, updateFrequencyMinutes: number) {
    updateUserFeed({ userId: this.#userId, feedId, updateFrequencyMinutes });
  }

  setFeedCopyFormat(feedId: number, copyFormat?: CopyFormat) {
    updateUserFeed({
      userId: this.#userId,
      feedId,
      copyFormat: copyFormat ?? null,
    });
  }

  setFeedEnabled(feedId: number, enabled: boolean) {
    updateUserFeed({ userId: this.#userId, feedId, enabled });
  }

  saveSettings(settings: string) {
    updateUser({
      userId: this.#userId,
      settings,
    });
  }
}
