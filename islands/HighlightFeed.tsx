import IconAlertTriangle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/alert-triangle.tsx";
import IconCircle from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle.tsx";
import IconCircleDot from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/circle-dot.tsx";
import IconEye from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/eye.tsx";
import IconEyeOff from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/eye-off.tsx";
import { FunctionComponent } from "preact";
import Progress from "../components/Progress.tsx";
import listDisplayContext from "../lib/listDisplayContext.ts";
import useTooltip from "../lib/useTooltip.tsx";
import {
  highlightShowAllEntries,
  markFeedAsRead,
  markFeedAsUnread,
  refreshFeed,
  removeFeed,
  setFeedCopyFormat,
  setFeedCustomName,
  setFeedEnabled,
  setFeedUpdateFrequency,
} from "../state/operations.ts";
import { Feed } from "../state/State.ts";
import useAppState from "../state/useAppState.ts";
import Button from "./Button.tsx";
import Input from "./Input.tsx";
import ListFeedEntry from "./ListFeedEntry.tsx";
import RelativeTime from "./RelativeTime.tsx";
import Select from "./Select.tsx";
import Link from "./Link.tsx";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

interface HighlightFeedProps {
  feed: Feed;
}

const HighlightFeed: FunctionComponent<HighlightFeedProps> = function ({
  feed,
}) {
  const { removingFeed, refreshingFeeds, highlightedShowAllEntries, dispatch } =
    useAppState();

  let { feedId, items } = feed;

  const isRemoving = removingFeed.value?.state === "in-progress" &&
    removingFeed.value.feedId === feedId;

  const isRefreshing = refreshingFeeds.value.includes(feedId);

  const resetTooltip = useTooltip(`Replace custom name '${feed.name}'`);

  const toggleEnabledTooltip = useTooltip(
    `${feed.enabled ? "Disable" : "Enable"} automatic feed updates`,
  );
  const ToggleEnabledIcon = feed.enabled ? IconEye : IconEyeOff;
  const toggleEnabledText = `Feed ${feed.enabled ? "enabled" : "disabled"}`;

  const count = items.length;
  const readCount = items.reduce((sum, a) => sum + (a.isRead ? 1 : 0), 0);
  const unreadCount = count - readCount;

  const isListTruncated = !highlightedShowAllEntries.value && items.length > 10;
  if (isListTruncated) {
    items = items.slice(0, 10);
  }

  return (
    <div class={isRemoving ? "opacity-60" : undefined} {...keyboardProps.grid}>
      <div class="mb-4">
        <Input
          variant="transparent"
          value={feed.name}
          onInput={(e) => {
            dispatch(setFeedCustomName, feedId, e.currentTarget.value);
          }}
        />
      </div>
      <div class="flex gap-4 mb-4" {...keyboardProps.gridRow}>
        <Button
          variant="secondary"
          onClick={() => {
            if (confirm(`Unsubscribe from ${feed.name}?`)) {
              dispatch(removeFeed, feedId);
            }
          }}
          disabled={isRemoving}
        >
          Unsubscribe...
          {isRemoving && <Progress class="ml-2" />}
        </Button>

        <Button
          variant="secondary"
          onClick={() => {
            dispatch(refreshFeed, feedId);
          }}
          disabled={isRefreshing}
        >
          Refresh
          {isRefreshing && <Progress class="ml-2" />}
        </Button>
      </div>
      {feed.lastError && feed.lastErrorDateTime
        ? (
          <>
            <div class="border border-yellow-400 p-4 mb-4">
              <div>
                <IconAlertTriangle class="inline-block text-yellow-400" />{" "}
                Error: {feed.lastError}
              </div>
              <div>
                Last attempted update:{" "}
                <RelativeTime time={feed.lastErrorDateTime} />
              </div>
            </div>
            <div>
              Last successful update:{" "}
              <RelativeTime time={feed.lastUpdatedDateTime} />
            </div>
          </>
        )
        : (
          <div>
            Last update: <RelativeTime time={feed.lastUpdatedDateTime} />
          </div>
        )}
      <div class={feed.enabled ? undefined : "opacity-60"}>
        Update frequency:{" "}
        <Select
          variant="transparent"
          value={feed.updateFrequencyMinutes}
          options={[
            { value: 60, label: "60 minutes" },
            { value: 60 * 6, label: "6 hours" },
            { value: 60 * 24, label: "24 hours" },
            { value: 60 * 24 * 7, label: "7 days" },
          ]}
          onOptionChange={(value) => {
            dispatch(setFeedUpdateFrequency, feedId, value);
          }}
        />
      </div>
      <div>
        Copy format:{" "}
        <Select
          variant="transparent"
          value={feed.copyFormat}
          options={[
            { value: undefined, label: "URL" },
            { value: "yt-dlp", label: "yt-dlp downloader" },
            {
              value: "yt-dlp-sound",
              label: "yt-dlp downloader (sound only)",
            },
          ] as const}
          onOptionChange={(value) => {
            dispatch(setFeedCopyFormat, feedId, value);
          }}
        />
      </div>
      <div>
        Feed added: <RelativeTime time={feed.addedDateTime} />
      </div>
      <div>Feed URL: {feed.url}</div> {feed.name !== feed.originalName && (
        <div>
          Original feed name: {feed.originalName} {resetTooltip.node}
          <Button
            ref={resetTooltip.ref}
            variant="tertiary"
            size="sm"
            onClick={() => {
              dispatch(setFeedCustomName, feedId, undefined);
            }}
          >
            Reset
          </Button>
        </div>
      )}
      <div {...keyboardProps.gridRow}>
        {toggleEnabledTooltip.node}
        <Link
          ref={toggleEnabledTooltip.ref}
          onClick={() =>
            dispatch(setFeedEnabled, feedId, !feed.enabled)}
        >
          <ToggleEnabledIcon
            size={"1em" as unknown as number}
            class="inline-block align-middle mr-2"
          />
          {toggleEnabledText}
        </Link>
      </div>
      <h3 class="text-xl mt-6 mb-4">Entries</h3>
      <div class="mb-2" {...keyboardProps.gridRow}>
        <Button
          variant="tertiary"
          onClick={() =>
            dispatch(unreadCount ? markFeedAsRead : markFeedAsUnread, feedId)}
          disabled={!unreadCount && !readCount}
        >
          {unreadCount > 0
            ? (
              <>
                <IconCircleDot
                  size={"1rem" as unknown as number}
                  class="inline-block align-middle mr-2"
                />{" "}
                Mark {unreadCount} as read
              </>
            )
            : (
              <>
                <IconCircle
                  size={"1rem" as unknown as number}
                  class="inline-block align-middle mr-2"
                />{" "}
                Mark {readCount} as unread
              </>
            )}
        </Button>
      </div>
      <listDisplayContext.Provider value={{ feeds: "all", entries: "all" }}>
        <ul>
          {items.map((x) => (
            <li key={x.feedItemId} {...keyboardProps.gridRow}>
              <ListFeedEntry
                feed={feed}
                feedEntry={x}
              />
            </li>
          ))}
          {isListTruncated && (
            <li class="italic ml-14" {...keyboardProps.gridRow}>
              <Link
                onClick={() =>
                  dispatch(highlightShowAllEntries)}
              >
                Show all {count} entries
              </Link>
            </li>
          )}
        </ul>
      </listDisplayContext.Provider>
    </div>
  );
};

export default HighlightFeed;
