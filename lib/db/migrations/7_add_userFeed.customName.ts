export default {
  id: "7_add_userFeed.customName",
  sql: `
    ALTER TABLE userFeed
    ADD COLUMN customName TEXT NULL;
    `,
};
