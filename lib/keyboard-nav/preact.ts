import { createContext, RefObject } from "preact";
import { useContext, useEffect } from "preact/hooks";
import {
  ATTR_FOCUSABLE,
  ATTR_GRID_ROW,
  ATTR_GROUP,
  GROUP_TYPE_GRID,
  GROUP_TYPE_HORIZONTAL,
  GROUP_TYPE_VERTICAL,
} from "./constants.ts";
import { KeyboardNav, KeyboardNavOptions } from "./mod.ts";

const keyboardNavContext = createContext<RefObject<KeyboardNav>>({
  current: null,
});

export const useKeyboardNav = (options?: KeyboardNavOptions) => {
  const ctx = useContext(keyboardNavContext);
  useEffect(() => {
    if (ctx.current) {
      console.warn("useKeyboardNav should only be called in one place");
      return;
    }

    const nav = new KeyboardNav(options);
    nav.enable();
    ctx.current = nav;

    return () => {
      nav.disable();
      ctx.current = null;
    };
  }, [ctx, options]);
};

export const keyboardProps = {
  focusable: { [ATTR_FOCUSABLE]: "" },
  grid: { [ATTR_GROUP]: GROUP_TYPE_GRID },
  gridRow: { [ATTR_GRID_ROW]: "" },
  horizontal: { [ATTR_GROUP]: GROUP_TYPE_HORIZONTAL },
  vertical: { [ATTR_GROUP]: GROUP_TYPE_VERTICAL },
};
