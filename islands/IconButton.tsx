import { FunctionComponent, JSX } from "preact";
import { forwardRef } from "preact/compat";
import { IS_BROWSER } from "$fresh/runtime.ts";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

export interface IconButtonProps
  extends Omit<JSX.HTMLAttributes<HTMLButtonElement>, "icon" | "size"> {
  icon: FunctionComponent<{ size: number }>;
  active?: boolean;
}

const IconButton: FunctionComponent<IconButtonProps> = forwardRef<
  HTMLButtonElement,
  IconButtonProps
>(function (
  { class: className, icon: Icon, active = false, type, disabled, ...rest },
  ref,
) {
  return (
    <button
      ref={ref}
      type={type ?? "button"}
      disabled={!IS_BROWSER || disabled}
      class={`p-1 ${
        {
          true: "text-blue-400 bg-black",
          false: "",
        }[`${active}`]
      } shadow(md lg(hover:& focus:& active:&)) rounded focus:(outline-none ring) transition duration-150 ease-in-out disabled:opacity-60 ${
        className ?? ""
      }`}
      {...keyboardProps.focusable}
      {...rest}
    >
      <Icon size={"1em" as unknown as number} />
    </button>
  );
});

export default IconButton;
