import { IS_BROWSER } from "$fresh/runtime.ts";
import { JSX } from "preact";
import { useId } from "preact/hooks";
import Input from "./Input.tsx";

interface TextFieldProps extends JSX.HTMLAttributes<HTMLInputElement> {
  label: string;
}

export default function TextField({
  label,
  class: className,
  id,
  ...rest
}: TextFieldProps) {
  const generatedId = useId();
  id ??= IS_BROWSER ? generatedId : undefined;
  return (
    <div class={className}>
      <label class="block" for={id}>
        {label}
      </label>
      <Input id={id} {...rest} />
    </div>
  );
}
