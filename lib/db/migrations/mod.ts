import migration1 from "./1_create_tables.ts";
import migration2 from "./2_add_userFeed.sort.ts";
import migration3 from "./3_add_feedItem.title.ts";
import migration4 from "./4_rename_feed.lastUpdateDateTime_to_lastUpdatedDateTime.ts";
import migration5 from "./5_add_userFeed.isOpen.ts";
import migration6 from "./6_rename_userFeed.isOpen_to_userFeed.expanded.ts";
import migration7 from "./7_add_userFeed.customName.ts";
import migration8 from "./8_add_user.settings.ts";
import migration9 from "./9_add_feed.lastError_and_feed.lastErrorDateTime.ts";
import migration10 from "./10_add_userFeed.enabled.ts";
import migration11 from "./11_add_userFeed.copyFormat.ts";

export default [
  migration1,
  migration2,
  migration3,
  migration4,
  migration5,
  migration6,
  migration7,
  migration8,
  migration9,
  migration10,
  migration11,
] satisfies Migration[];

export interface Migration {
  id: string;
  sql: string;
}
