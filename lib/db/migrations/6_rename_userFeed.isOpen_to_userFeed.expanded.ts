export default {
  id: "6_rename_userFeed.isOpen_to_userFeed.expanded",
  sql: `
    ALTER TABLE userFeed
    RENAME COLUMN isOpen TO expanded;
    `,
};
