import { FunctionComponent } from "preact";

interface CenterProps {
  class?: string;
}

const Center: FunctionComponent<CenterProps> = ({
  class: className,
  children,
}) => {
  return (
    <div class={`flex flex-col items-center justify-center ${className ?? ""}`}>
      <div class="flex-grow" />
      {children}
      <div class="flex-grow-[2]" />
    </div>
  );
};

export default Center;
