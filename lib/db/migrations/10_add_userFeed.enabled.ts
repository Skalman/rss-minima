export default {
  id: "10_add_userFeed.enabled",
  sql: `
    ALTER TABLE userFeed
    ADD COLUMN enabled INTEGER NOT NULL DEFAULT 1;
    `,
};
