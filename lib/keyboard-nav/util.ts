export function isElement(x: unknown): x is HTMLElement {
  return x instanceof HTMLElement;
}

export function findAll(
  container: HTMLElement,
  selector: string,
): HTMLElement[] {
  return [...container.querySelectorAll(selector)] as HTMLElement[];
}

export function findAllVisible(
  container: HTMLElement,
  selector: string,
): HTMLElement[] {
  return findAll(container, selector).filter(isElementVisible);
}

const focusableSelector =
  ":is(a, area)[href], :is(input, select, textarea, button):enabled, iframe, object, embed, [tabindex], [contenteditable]";

export function findAllFocusable(
  container: HTMLElement,
  selector: string,
): HTMLElement[] {
  return findAll(container, `:is(${selector}):is(${focusableSelector})`).filter(
    isElementVisible,
  );
}

export function isElementVisible(element: HTMLElement): boolean {
  if (!element.offsetParent) return false;

  const style = getComputedStyle(element);
  if (style.display === "none" || style.visibility === "hidden") return false;

  return true;
}

/**
 * Determine whether an element can be focused.
 */
export function isElementFocusable(element: HTMLElement): boolean {
  return isElementVisible(element) && element.matches(focusableSelector);
}

export function arrayAt<T>(
  array: T[],
  index: number,
  allowWraparound: boolean,
): T | undefined {
  return allowWraparound
    ? array[(index + array.length) % array.length]
    : array[index];
}

export class Lazy<T> {
  #init?: () => T;
  #value?: T;

  get value() {
    if (this.#init) {
      this.#value = this.#init();
      this.#init = undefined;
    }

    return this.#value as T;
  }

  get valueIfInitialized() {
    return this.#value;
  }

  constructor(init: () => T) {
    this.#init = init;
  }
}
