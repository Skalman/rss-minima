import { useContext } from "preact/hooks";
import appStateContext from "../state/appStateContext.ts";

export default function useAppState() {
  return useContext(appStateContext);
}
