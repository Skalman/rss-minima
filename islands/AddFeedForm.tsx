import { FunctionComponent } from "preact";
import { addFeed } from "../state/operations.ts";
import useAppState from "../state/useAppState.ts";
import Button from "./Button.tsx";
import TextField from "./TextField.tsx";
import { useSignal } from "@preact/signals";
import Alert from "../components/Alert.tsx";
import CopyButton from "./CopyButton.tsx";
import Typography from "./Typography.tsx";

async function rss() {
  const div = document.createElement("div");
  div.innerHTML = await (await fetch(location.href)).text();

  const find = (
    selector: string,
    getProp: (
      x: Partial<HTMLLinkElement> & Partial<HTMLMetaElement>,
    ) => string | undefined,
  ) =>
    Array.from(div.querySelectorAll(selector))
      .map(getProp)
      .filter((x): x is string => !!x);

  let urls = find(
    "link[rel=alternate][type='application/rss+xml']",
    (x) => x.href,
  );

  if (!urls.length) {
    const { href, host } = location;
    if (href.startsWith("https://www.youtube.com/playlist?list=")) {
      urls = [
        `https://www.youtube.com/feeds/videos.xml?playlist_id=${
          href.match(/list=([^&]+)/)?.[1]
        }`,
      ];
    } else if (host === "soundcloud.com") {
      urls = find(
        "meta[property='twitter:app:url:googleplay']",
        (x) =>
          x.content?.startsWith("soundcloud://users:")
            ? `https://feeds.soundcloud.com/users/soundcloud:${
              x.content.replace(
                "soundcloud://",
                "",
              )
            }/sounds.rss`
            : "",
      );
    } else {
      urls = ["RSS feed not found"];
    }
  }

  console.log(urls.join("\n"));
}

const AddFeedForm: FunctionComponent = function () {
  const {
    addFeed: { submission },
    dispatch,
  } = useAppState();

  const url = useSignal("");

  const isSubmitting = submission.value?.state === "in-progress";
  const error = submission.value?.state === "error" && submission.value.error;

  const script = `${rss}await ${rss.name}()`;

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        dispatch(addFeed, url.value);
      }}
    >
      <Typography variant="h3" class="mb-2">
        Add feed
      </Typography>

      <TextField
        label="URL"
        value={url.value}
        onInput={(e) => {
          url.value = e.currentTarget.value;
        }}
        disabled={isSubmitting}
      />
      <Button variant="primary" type="submit" disabled={isSubmitting}>
        Add feed
      </Button>
      {error && (
        <Alert variant="error">
          Error:{" "}
          {error instanceof Error ? error.message : JSON.stringify(error)}
        </Alert>
      )}
      <hr class="my-4 border-gray-500" />
      <div class="flex gap-4">
        <CopyButton
          title="Copy script to find URL from a webpage"
          text={script}
        />
        <pre class="w-0 flex-grow h-6 overflow-hidden">{script}</pre>
      </div>
    </form>
  );
};

export default AddFeedForm;
