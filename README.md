# RSS Minima

<img src="./static/logo.svg" width="16" height="16">
Simple self-hostable RSS reader written in Deno.

## Usage

Start the project in development mode, watching for file changes:

```
deno task start
```

Start the project in production mode:

```
deno run -A main.ts
```
