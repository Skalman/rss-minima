import { IS_BROWSER } from "$fresh/runtime.ts";
import { FunctionComponent, JSX } from "preact";
import { forwardRef } from "preact/compat";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

export type LinkProps = JSX.HTMLAttributes<HTMLAnchorElement>;

const Link: FunctionComponent<LinkProps> = forwardRef<
  HTMLAnchorElement,
  LinkProps
>(function ({ class: className, href, onClick, ...rest }, ref) {
  let isDisabled = false;
  if (href === undefined) {
    if (IS_BROWSER) {
      href = "#";
      const originalOnClick = onClick;
      onClick = function (this: HTMLAnchorElement, e) {
        e.preventDefault();
        originalOnClick?.call(this, e);
      };
    } else {
      isDisabled = true;
    }
  }

  return (
    <a
      ref={ref}
      href={href}
      class={`py-1 underline(hover:& focus:& active:&) shadow(md lg(hover:& focus:& active:&)) rounded focus:(outline-none ring) transition duration-150 ease-in-out ${
        isDisabled ? "opacity-60" : ""
      } ${className ?? ""}`}
      onClick={onClick}
      {...keyboardProps.focusable}
      {...rest}
    />
  );
});

export default Link;
