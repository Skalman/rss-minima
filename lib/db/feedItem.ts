import database from "./database.ts";

export interface FeedItem {
  feedItemId: number;
  feedId: number;
  title: string;
  url: string;
  feedItemDateTime: string;
  content: string | null;
  attachmentUrl: string | null;
}

/**
 * Returns the `feedItemId`s in the same order as the feed items were given.
 */
export function upsertFeedItems(
  feedId: number,
  feedItems: {
    title: string;
    url: string;
    feedItemDateTime: string;
    content?: string;
    attachmentUrl?: string;
  }[],
) {
  const existingItems = new Map(
    database
      .queryEntries<{
        feedItemId: number;
        title: string;
        url: string;
        feedItemDateTime: string;
        content: string | null;
        attachmentUrl: string | null;
      }>(
        `
          SELECT
            feedItemId,
            title,
            url,
            feedItemDateTime,
            content,
            attachmentUrl
          FROM feedItem
          WHERE feedId = :feedId
        `,
        { feedId },
      )
      .map((x) => [x.url, x]),
  );

  for (
    const {
      url,
      title,
      feedItemDateTime,
      content,
      attachmentUrl,
    } of feedItems
  ) {
    const existing = existingItems.get(url);
    if (existing) {
      database
        .prepareQuery(
          `
            UPDATE feedItem
            SET
              title = :title,
              feedItemDateTime = :feedItemDateTime,
              content = :content,
              attachmentUrl = :attachmentUrl
            WHERE feedItemId = :feedItemId`,
        )
        .execute({
          title,
          feedItemDateTime,
          content,
          attachmentUrl,
          feedItemId: existing.feedItemId,
        });
    } else {
      database
        .prepareQuery(
          `
            INSERT INTO feedItem (feedId, title, url, feedItemDateTime, content, attachmentUrl)
            VALUES (:feedId, :title, :url, :feedItemDateTime, :content, :attachmentUrl)
          `,
        )
        .execute({
          feedId,
          url,
          title,
          feedItemDateTime,
          content,
          attachmentUrl,
        });
    }
  }

  const newUrls = new Set(feedItems.map((x) => x.url));
  for (const { url, feedItemId } of existingItems.values()) {
    if (!newUrls.has(url)) {
      database
        .prepareQuery(
          `
            DELETE FROM userFeedItemRead
            WHERE feedItemId = :feedItemId;`,
        )
        .execute({ feedItemId });

      database
        .prepareQuery(
          `
            DELETE FROM feedItem
            WHERE feedItemId = :feedItemId;`,
        )
        .execute({ feedItemId });
    }
  }
}
