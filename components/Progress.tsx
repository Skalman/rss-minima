import { FunctionComponent, JSX } from "preact";
import IconLoader2 from "https://deno.land/x/tabler_icons_tsx@0.0.2/tsx/loader-2.tsx";

interface ProgressProps
  extends Omit<JSX.HTMLAttributes<SVGSVGElement>, "size"> {
  size?: string | number;
}

const Progress: FunctionComponent<ProgressProps> = function ({
  class: className,
  size = "1em",
  ...rest
}) {
  return (
    <IconLoader2
      size={size as unknown as number}
      class={`animate-spin inline-block ${className}`}
      {...rest}
    />
  );
};

export default Progress;
