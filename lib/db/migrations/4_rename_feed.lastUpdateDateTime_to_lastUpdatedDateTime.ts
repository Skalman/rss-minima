export default {
  id: "4_rename_feed.lastUpdateDateTime_to_lastUpdatedDateTime",
  sql: `
    ALTER TABLE feed
    RENAME COLUMN lastUpdateDateTime TO lastUpdatedDateTime
    `,
};
