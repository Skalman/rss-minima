import { JSX } from "preact";
import { IS_BROWSER } from "$fresh/runtime.ts";

export interface InputProps extends JSX.HTMLAttributes<HTMLInputElement> {
  variant?: "default" | "transparent";
}

const Input = function ({ variant = "default", ...rest }: InputProps) {
  return (
    <input
      {...rest}
      disabled={!IS_BROWSER || rest.disabled}
      class={`px-3 py-2 bg(gray-700 focus:gray-600) text-white rounded border(gray-500 2) disabled:(opacity-50 cursor-not-allowed) ${
        {
          default: "",
          transparent:
            "not-hover:not-focus:(bg-transparent border-transparent)",
        }[variant]
      } ${rest.class ?? ""}`}
    />
  );
};

export default Input;
