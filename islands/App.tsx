import { useEffect } from "preact/hooks";
import { FunctionComponent } from "preact";
import { refreshFeed, toggleAddFeedForm } from "../state/operations.ts";
import useAppState from "../state/useAppState.ts";
import Button from "./Button.tsx";
import List from "./List.tsx";
import Highlight from "./Highlight.tsx";
import ButtonGroupRadio from "./ButtonGroupRadio.tsx";
import { ListDisplay } from "../state/State.ts";
import Logo from "./Logo.tsx";
import { keyboardProps } from "../lib/keyboard-nav/preact.ts";

const getListDisplayKey = (x: ListDisplay) => JSON.stringify(x);
const getListDisplay = (x: string): ListDisplay => JSON.parse(x);
const listDisplayOptions: { value: string; label: string }[] = [
  {
    label: "All",
    value: getListDisplayKey({ feeds: "all", entries: "all" }),
  },
  {
    label: "Feeds + Unread",
    value: getListDisplayKey({ feeds: "all", entries: "unread" }),
  },
  {
    label: "Unread only",
    value: getListDisplayKey({ feeds: "unread", entries: "unread" }),
  },
];

const App: FunctionComponent = function () {
  const { listDisplay, nextFeedRefresh, dispatch } = useAppState();

  useEffect(() => {
    const nextRefresh = nextFeedRefresh.value;
    if (!nextRefresh) {
      return;
    }

    const timeFromNow = nextRefresh.refreshTimestamp - Date.now();

    const timeout = setTimeout(() => {
      dispatch(refreshFeed, nextRefresh.feed.feedId);
    }, timeFromNow);

    return () => {
      clearTimeout(timeout);
    };
  }, [dispatch, nextFeedRefresh.value]);

  return (
    <div class="h-full flex flex-col gap-4 pt-4">
      <div class="px-4 flex gap-4 items-center" {...keyboardProps.horizontal}>
        <Logo class="h-8 w-8" />

        <ButtonGroupRadio
          value={getListDisplayKey(listDisplay.value)}
          options={listDisplayOptions}
          onOptionChange={(x) => {
            listDisplay.value = getListDisplay(x);
          }}
        />

        <Button variant="tertiary" onClick={() => dispatch(toggleAddFeedForm)}>
          Add
        </Button>
      </div>

      <div class="h-0 flex-grow flex gap-4">
        <div class="pl-4 pb-8 w-0 flex-1 max-w-[35rem] overflow-auto">
          <List />
        </div>
        <div class="pr-4 pb-8 w-0 flex-1 overflow-auto">
          <Highlight />
        </div>
      </div>
    </div>
  );
};

export default App;
